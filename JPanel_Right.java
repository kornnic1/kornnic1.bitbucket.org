import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.util.*;
import java.util.Collections; 
import java.net.MalformedURLException;
import java.net.*;
import javax.imageio.ImageIO;


public class JPanel_Right extends JPanel implements ActionListener, DocumentListener, ItemListener{
    //variables for jpanel
    private  JPanel j_panel_right_content; 
    private  JPanel j_panel_right_content_bottom;
    private  JPanel j_panel_right_content_top;
    private  JPanel j_panel_right_content_middle;
    private  JPanel j_panel_right_content_middle_pic_flow;
    
    //Labels
    private  JLabel lbl_step;
    private  JLabel lbl_step_explanation;
    private  JLabel lbl_empty;
    private  JLabel lbl_result;

    //Drop down menus
    private  JComboBox<Object>        cb_dropdown_1;
    private  JComboBox<Object>        cb_dropdown_2;    
    private  JComboBox<Object>        cb_dropdown_3;    
    
    //Textfields
    private  JTextField   tf_textfield_1;
    private  JTextField   tf_textfield_2;
    private  JTextField   tf_textfield_result;
    
    //Strings
    private  String str_step;
    private  String str_step_explanation;
    private  String str_dropdown_1;
    private  String str_dropdown_2;
    private  String str_dropdown_3;
    private  String str_textfield_1;
    private  int i_component_size_x;
    private  int i_component_size_y;
    
    //Button pictures for installation mode
    private  ImageButton ib_tray_1l;
    private  ImageIcon icon_tray_1l;
    private  ImageButton ib_tray_2l;
    private  ImageIcon icon_tray_2l;
    private  ImageButton ib_tray_2l_2t;
    private  ImageIcon icon_tray_2l_2t;
    private  ImageButton ib_tray_4l;
    private  ImageIcon icon_tray_4l;
    private  ImageButton ib_air;
    private  ImageIcon icon_air;
    private  ImageButton ib_ceiling;
    private  ImageIcon icon_ceiling;
    private  ImageButton ib_floor_wall;
    private  ImageIcon icon_floor_wall;
    private  ImageButton ib_pipe;
    private  ImageIcon icon_pipe;
    private  int i_pic_button_size_x;
    private  int i_pic_button_size_y;
    
    //Buttons
    private  JButton btn_tray_1l;
    private  JButton btn_tray_2l;
    private  JButton btn_tray_2l_2t;
    private  JButton btn_tray_4l;
    private  JButton btn_air;
    private  JButton btn_ceiling;
    private  JButton btn_floor_wall;
    private  JButton btn_pipe;
    private  JButton btn_test;
    
    private  boolean bol_pic_button_selected;    
    
    //general variables
    private  int i;
    
    public JPanel_Right(String str_step, String str_step_explanation, ItemListener item_listener, ActionListener action_listener, DocumentListener document_listener){
         //////////////////////Right side of the applet GUI///////////////////////////////////////
         
         this.str_step = str_step;
         this.str_step_explanation = str_step_explanation;
         this.setBackground(new Color(164, 185, 204));
         
         //Layout for content in right panel
         j_panel_right_content = new JPanel();
         //j_panel_right_content.setLayout(new BoxLayout(j_panel_right_content,BoxLayout.PAGE_AXIS));
         //j_panel_right_content.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Recent Step"));
         //j_panel_right_content.setBackground(new Color(164, 185, 204));
         j_panel_right_content.setLayout(new BorderLayout());
         j_panel_right_content.setSize(400, 390);
         j_panel_right_content.setPreferredSize(new Dimension(400, 390));
         j_panel_right_content.setMinimumSize(new Dimension(400, 390));
         j_panel_right_content.setMaximumSize(new Dimension(400, 390));
         
         //Top Panel
         j_panel_right_content_top = new JPanel();
         j_panel_right_content_top.setLayout(new BoxLayout(j_panel_right_content_top,BoxLayout.PAGE_AXIS));
         j_panel_right_content_top.setBackground(new Color(164, 185, 204));
         j_panel_right_content.add(j_panel_right_content_top, BorderLayout.PAGE_START);
         
         //init Labels for top panel
         //lbl_step = new JLabel(str_step);
         //lbl_step.setAlignmentX(Component.CENTER_ALIGNMENT);
         //lbl_step.setFont(lbl_step.getFont().deriveFont(Font.BOLD,18.0f));
         //j_panel_right_content_top.add(lbl_step);
         lbl_empty = new JLabel("<html><body> &nbsp <br> &nbsp </body></html>");
         lbl_empty.setAlignmentX(Component.CENTER_ALIGNMENT);
         j_panel_right_content_top.add(lbl_empty);
         lbl_step_explanation = new JLabel(str_step_explanation);
         lbl_step_explanation.setFont(lbl_step_explanation.getFont().deriveFont(Font.BOLD,18.0f));
         lbl_step_explanation.setAlignmentX(Component.CENTER_ALIGNMENT);
         j_panel_right_content_top.add(lbl_step_explanation);
         lbl_empty = new JLabel("<html><body> &nbsp <br> &nbsp </body></html>");
         lbl_empty.setAlignmentX(Component.CENTER_ALIGNMENT);
         j_panel_right_content_top.add(lbl_empty);

         

         //Center Panel
         i_component_size_x = 300;
         i_component_size_y = 25;
         
         j_panel_right_content_middle = new JPanel();
         j_panel_right_content_middle.setLayout(new BoxLayout(j_panel_right_content_middle,BoxLayout.PAGE_AXIS));
         j_panel_right_content_middle.setBackground(new Color(164, 185, 204));
         j_panel_right_content.add(j_panel_right_content_middle, BorderLayout.CENTER);
         
         //init dropdowns on center panel
         //init cb_dropdown_1 list
         cb_dropdown_1 = new JComboBox<Object>();
         cb_dropdown_1.setMinimumSize(new Dimension(i_component_size_x,i_component_size_y));
         cb_dropdown_1.setMaximumSize(new Dimension(i_component_size_x,i_component_size_y));
         j_panel_right_content_middle.add(cb_dropdown_1);
         cb_dropdown_1.addItemListener(this);
         cb_dropdown_1.addItemListener(item_listener);
         cb_dropdown_1.addActionListener(this);
         cb_dropdown_1.addActionListener(action_listener);
         
         //Empty line between the 2 dropdownlists
         lbl_empty = new JLabel("<html><body> &nbsp <br> &nbsp </body></html>");
         j_panel_right_content_top.add(lbl_empty, BorderLayout.CENTER);
         j_panel_right_content_middle.add(lbl_empty);
         
         //init cb_dropdown_2 list
         cb_dropdown_2 = new JComboBox<Object>();
         cb_dropdown_2.setMinimumSize(new Dimension(i_component_size_x,i_component_size_y));
         cb_dropdown_2.setMaximumSize(new Dimension(i_component_size_x,i_component_size_y));
         j_panel_right_content_middle.add(cb_dropdown_2);
         cb_dropdown_2.addItemListener(this);
         cb_dropdown_2.addItemListener(item_listener);
         cb_dropdown_2.addActionListener(this);
         cb_dropdown_2.addActionListener(action_listener);
         
         //init cb_dropdown_3 list
         cb_dropdown_3 = new JComboBox<Object>();
         cb_dropdown_3.setMinimumSize(new Dimension(i_component_size_x,i_component_size_y));
         cb_dropdown_3.setMaximumSize(new Dimension(i_component_size_x,i_component_size_y));
         j_panel_right_content_middle.add(cb_dropdown_3);
         cb_dropdown_3.addItemListener(this);
         cb_dropdown_3.addItemListener(item_listener);
         cb_dropdown_3.addActionListener(this);
         cb_dropdown_3.addActionListener(action_listener);
         
         //init input textfield
         tf_textfield_1 = new JTextField();
         tf_textfield_1.setMinimumSize(new Dimension(i_component_size_x,i_component_size_y));
         tf_textfield_1.setMaximumSize(new Dimension(i_component_size_x,i_component_size_y));
         j_panel_right_content_middle.add(tf_textfield_1);
         //tf_textfield_1.addActionListener(this);
         tf_textfield_1.getDocument().addDocumentListener(this);
         tf_textfield_1.getDocument().addDocumentListener(document_listener);
         
         //Pictured buttons for the installation mode step
         //middle anel with pics first line
         j_panel_right_content_middle_pic_flow = new JPanel();
         j_panel_right_content_middle_pic_flow.setLayout(new FlowLayout());
         j_panel_right_content_middle_pic_flow.setBackground(new Color(164, 185, 204));
         j_panel_right_content_middle.add(j_panel_right_content_middle_pic_flow);
         
         //init pic buttons
         try{
             i_pic_button_size_x = 95;
             i_pic_button_size_y = 95;
             
             ib_tray_1l = new ImageButton();
             icon_tray_1l = ib_tray_1l.icon_tray_1l();
             btn_tray_1l = new JButton(icon_tray_1l);
             btn_tray_1l.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_tray_1l.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_1l.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_1l.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_tray_1l);
             btn_tray_1l.addActionListener(this);
             btn_tray_1l.addActionListener(action_listener);
             
             ib_tray_2l = new ImageButton();
             icon_tray_2l = ib_tray_2l.icon_tray_2l();
             btn_tray_2l = new JButton(icon_tray_2l);
             btn_tray_2l.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_tray_2l.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_2l.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_2l.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_tray_2l);
             btn_tray_2l.addActionListener(this);
             btn_tray_2l.addActionListener(action_listener);
    
             ib_tray_2l_2t = new ImageButton();
             icon_tray_2l_2t = ib_tray_2l_2t.icon_tray_2l_2t();
             btn_tray_2l_2t = new JButton(icon_tray_2l_2t);
             btn_tray_2l_2t.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_tray_2l_2t.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_2l_2t.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_2l_2t.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_tray_2l_2t);
             btn_tray_2l_2t.addActionListener(this);
             btn_tray_2l_2t.addActionListener(action_listener);
             
             ib_tray_4l = new ImageButton();
             icon_tray_4l = ib_tray_4l.icon_tray_4l();
             btn_tray_4l = new JButton(icon_tray_4l);
             btn_tray_4l.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_tray_4l.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_4l.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_tray_4l.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_tray_4l);
             btn_tray_4l.addActionListener(this);
             btn_tray_4l.addActionListener(action_listener);
             
             ib_air = new ImageButton();
             icon_air = ib_air.icon_air();
             btn_air = new JButton(icon_air);
             btn_air.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_air.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_air.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_air.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_air);
             btn_air.addActionListener(this);
             btn_air.addActionListener(action_listener);
             
             ib_ceiling = new ImageButton();
             icon_ceiling = ib_ceiling.icon_ceiling();
             btn_ceiling = new JButton(icon_ceiling);
             btn_ceiling.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_ceiling.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_ceiling.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_ceiling.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_ceiling);
             btn_ceiling.addActionListener(this);
             btn_ceiling.addActionListener(action_listener);
             
             ib_floor_wall = new ImageButton();
             icon_floor_wall = ib_floor_wall.icon_floor_wall();
             btn_floor_wall = new JButton(icon_floor_wall);
             btn_floor_wall.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_floor_wall.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_floor_wall.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_floor_wall.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_floor_wall);
             btn_floor_wall.addActionListener(this);
             btn_floor_wall.addActionListener(action_listener);
             
             ib_pipe = new ImageButton();
             icon_pipe = ib_pipe.icon_pipe();
             btn_pipe = new JButton(icon_pipe);
             btn_pipe.setSize(i_pic_button_size_x, i_pic_button_size_y);
             btn_pipe.setPreferredSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_pipe.setMinimumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             btn_pipe.setMaximumSize(new Dimension(i_pic_button_size_x, i_pic_button_size_y));
             j_panel_right_content_middle_pic_flow.add(btn_pipe);
             btn_pipe.addActionListener(this);
             btn_pipe.addActionListener(action_listener);
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null,ex);
         } 
         
         
         
         //Bottom Panel
         j_panel_right_content_bottom = new JPanel();
         j_panel_right_content_bottom.setLayout(new BoxLayout(j_panel_right_content_bottom,BoxLayout.PAGE_AXIS));
         j_panel_right_content_bottom.setBackground(new Color(164, 185, 204));
         j_panel_right_content.add(j_panel_right_content_bottom, BorderLayout.PAGE_END);
         
         //result output field
         //lbl_result = new JLabel();
         //lbl_result.setAlignmentX(Component.CENTER_ALIGNMENT);
         //j_panel_right_content_top.add(lbl_result);
         tf_textfield_result = new JTextField("Result");
         //tf_textfield_result.setMaximumSize(new Dimension(269, 40));
         tf_textfield_result.setMinimumSize(new Dimension(i_component_size_x,i_component_size_y+10));
         tf_textfield_result.setMaximumSize(new Dimension(i_component_size_x,i_component_size_y+10));
         tf_textfield_result.setHorizontalAlignment(SwingConstants.CENTER);
         tf_textfield_result.setEnabled(true);
         tf_textfield_result.setEditable(false);
         tf_textfield_result.setBackground(new Color(0, 105, 204));
         tf_textfield_result.setForeground(Color.WHITE);
         tf_textfield_result.setFont(lbl_step_explanation.getFont().deriveFont(Font.BOLD,18.0f));
         j_panel_right_content_bottom.add(tf_textfield_result);
         
         add(j_panel_right_content);
         this.setVisible(true);
    }
    
    public void load_combobox_1(ArrayList<String> al_dropdown_content){
        //load data into cb_dropdown_1 list
        cb_dropdown_1.setModel(new DefaultComboBoxModel());
        for(i=0 ; i<al_dropdown_content.size() ; i++){
            cb_dropdown_1.addItem(al_dropdown_content.get(i));
        }
    }
    
    public void load_combobox_2(ArrayList<String> al_dropdown_content){
        //load data into cb_dropdown_2 list
        cb_dropdown_2.setModel(new DefaultComboBoxModel());
        for(i=0 ; i<al_dropdown_content.size() ; i++){
            cb_dropdown_2.addItem(al_dropdown_content.get(i));
        }
    }
    
    public void load_combobox_3(ArrayList<String> al_dropdown_content){
        //load data into cb_dropdown_2 list
        cb_dropdown_3.setModel(new DefaultComboBoxModel());
        for(i=0 ; i<al_dropdown_content.size() ; i++){
            cb_dropdown_3.addItem(al_dropdown_content.get(i));
        }
    }
    
    public void set_combobox_1(String str_set_combobox){
        cb_dropdown_1.setSelectedItem(str_set_combobox);
    }
    
    public void set_combobox_1_visibility(Boolean bol_visibility){
        cb_dropdown_1.setVisible(bol_visibility);
    }
    
    public void set_combobox_2(String str_set_combobox){
        cb_dropdown_2.setSelectedItem(str_set_combobox);
    }
    
    public void set_combobox_2_visibility(Boolean bol_visibility){
        cb_dropdown_2.setVisible(bol_visibility);
    }
    
    public void set_combobox_3(String str_set_combobox){
        cb_dropdown_3.setSelectedItem(str_set_combobox);
    }
    
    public void set_combobox_3_visibility(Boolean bol_visibility){
        cb_dropdown_3.setVisible(bol_visibility);
    }
    
    public void set_panel_vibility(Boolean bol_visibility){
        j_panel_right_content.setVisible(bol_visibility);
    }
    
    public void set_textfield_1_visibility(Boolean bol_visibility){
        tf_textfield_1.setVisible(bol_visibility);
    }
    
    public void set_textfield_result_enabled(Boolean bol_enabled){
        tf_textfield_result.setEnabled(bol_enabled);
    }
    
    public void set_textfield_result(String str_result){
        tf_textfield_result.setText(str_result);
    }
    
    public void set_textfield_color(String str_color){
        if(str_color == "blue"){
            tf_textfield_result.setBackground(new Color(0, 105, 204));
        }else{
            tf_textfield_result.setBackground(Color.RED);
        }
    }
    
    public String get_dropdown_1(){
        return str_dropdown_1;
    }
    
    public String get_dropdown_2(){
        return str_dropdown_2;
    }
    
    public String get_dropdown_3(){
        return str_dropdown_3;
    }
    
    public void remove_combobox_1_item(String str_item){
        cb_dropdown_1.removeItem(str_item);
    }
    
    public void remove_combobox_2_item(String str_item){
        cb_dropdown_2.removeItem(str_item);
    }
    
    public void remove_combobox_3_item(String str_item){
        cb_dropdown_2.removeItem(str_item);
    }
    
    public void set_inst_icon(String str_icon){
        activate_installation_mode_button(str_icon);
    }
    
    public String get_textfield_1(){
        return str_textfield_1;
    }
    
    /**
     * this method marks the sidebuttons to show the recent step 
     */
    public void activate_installation_mode_button(String inst_mode){
        switch(inst_mode){
            case "tray 1 layer":    
                            btn_tray_1l.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
            break;         
             
            case "tray 2 layer":   
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
            break;
                    
            case "2 trays with each 2 layers":   
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
            break;
                    
            case "tray stappled":
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null); 
            break;
                    
            case "air":
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
            break;
                    
            case "on ceiling":
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
            break;
                    
            case "on floor or wall":
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
                            btn_pipe.setBorder(null);
            break;
                    
            case "in a pipe":
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(new LineBorder(new Color(74, 155, 226),5,true));
            break;
    
            default:
                            btn_tray_1l.setBorder(null);
                            btn_tray_2l.setBorder(null);
                            btn_tray_2l_2t.setBorder(null);
                            btn_tray_4l.setBorder(null);
                            btn_air.setBorder(null);
                            btn_ceiling.setBorder(null);
                            btn_floor_wall.setBorder(null);
                            btn_pipe.setBorder(null);
        }
    }
    
    public void set_installation_mode_pics_visible(Boolean bool){
        btn_tray_1l.setVisible(bool);
        btn_tray_2l.setVisible(bool);
        btn_tray_2l_2t.setVisible(bool);
        btn_tray_4l.setVisible(bool);
        btn_air.setVisible(bool);
        btn_ceiling.setVisible(bool);
        btn_floor_wall.setVisible(bool);
        btn_pipe.setVisible(bool);
    }
    
    public void itemStateChanged(ItemEvent e){
        if(e.getStateChange() == ItemEvent.SELECTED){
            try{
                str_dropdown_1 = cb_dropdown_1.getSelectedItem().toString();
                str_dropdown_2 = cb_dropdown_2.getSelectedItem().toString();
            }catch(Exception h){
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        //set the borders for choosen installationmodes by clicking them
        if(e.getSource() == btn_tray_1l){
            activate_installation_mode_button("tray 1 layer");
            cb_dropdown_1.setSelectedItem("tray 1 layer");
        }
        if(e.getSource() == btn_tray_2l){
            activate_installation_mode_button("tray 2 layer");
            cb_dropdown_1.setSelectedItem("tray 2 layer");
        }
        if(e.getSource() == btn_tray_2l_2t){
            activate_installation_mode_button("2 trays with each 2 layers");
            cb_dropdown_1.setSelectedItem("2 trays with each 2 layers");
        }
        if(e.getSource() == btn_tray_4l){
            activate_installation_mode_button("tray stappled");
            cb_dropdown_1.setSelectedItem("tray stappled");
        }
        if(e.getSource() == btn_air){
            activate_installation_mode_button("air");
            cb_dropdown_1.setSelectedItem("air");
        }
        if(e.getSource() == btn_floor_wall){
            activate_installation_mode_button("on floor or wall");
            cb_dropdown_1.setSelectedItem("on floor or wall");
        }
        if(e.getSource() == btn_ceiling){
            activate_installation_mode_button("on ceiling");
            cb_dropdown_1.setSelectedItem("on ceiling");
        }
        if(e.getSource() == btn_pipe){
            activate_installation_mode_button("in a pipe");
            cb_dropdown_1.setSelectedItem("in a pipe");
        }
    }
    
    public void changedUpdate(DocumentEvent f) {
        System.out.println(tf_textfield_1.getText());
        str_textfield_1 = tf_textfield_1.getText();
    }

    public void insertUpdate(DocumentEvent f) {
        System.out.println(tf_textfield_1.getText());
        str_textfield_1 = tf_textfield_1.getText();
    }
    
    public void removeUpdate(DocumentEvent f) {
        System.out.println(tf_textfield_1.getText());
        str_textfield_1 = tf_textfield_1.getText();
    }
}
