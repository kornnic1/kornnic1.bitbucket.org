/**
 * This class builds cable objects
 */
public class Cable{
    //Variables
    private String     str_market_segment;
    private String     str_family;
    private String     str_current_crosssection;
    private String     str_current_crosssection_value;
    private String     str_sc_mc;
    private String     str_mc_core_count;
    private String     str_frequency;
    private String     str_conductor_temperature;
    private String     str_ambient_temperature;
    private String     str_ambient_installation_mode;
    private String     str_ambient_count;
    private String     str_result;
    
    /**
     * Constructor for objects of class LEDFXDISCO
     */
    public Cable(){
        str_market_segment = "choose";
        str_family = "no market has been choosen";
        str_current_crosssection = "choose";
        str_current_crosssection_value = "choose";
        str_sc_mc = "choose";
        str_mc_core_count = "choose";
        str_frequency = "choose";
        str_conductor_temperature = "choose";
        str_ambient_temperature = "choose";
        str_ambient_installation_mode = "choose";
        str_ambient_count = "choose";
        str_result = "choose";
    }

    //Setters////////////////////////////////////////////////////////////////////////////////////////////////
    public void set_market(String str_market_segment){
        this.str_market_segment = str_market_segment;
    }
    
    public void set_family(String str_family){
        this.str_family = str_family;
    }
    
    public void set_current_crosssection(String str_current_crosssection, String str_current_crosssection_value){
        this.str_current_crosssection = str_current_crosssection;
        this.str_current_crosssection_value = str_current_crosssection_value;
    }
    
    public void set_sc_mc(String str_sc_mc){
        this.str_sc_mc = str_sc_mc;
    }
    
    public void set_mc_core_number(String str_mc_core_count){
        this.str_mc_core_count = str_mc_core_count;
    }
    
    public void set_frequency(String str_frequency){
        this.str_frequency = str_frequency;
    }
    
    public void set_conductor_temperature(String str_conductor_temperature){
        this.str_conductor_temperature = str_conductor_temperature;
    }
    
    public void set_ambient_temperature(String str_ambient_temperature){
        this.str_ambient_temperature = str_ambient_temperature;
    }
    
    public void set_installation_mode(String str_ambient_installation_mode){
        this.str_ambient_installation_mode = str_ambient_installation_mode;
    }
    
    public void set_count(String str_ambient_count){
        this.str_ambient_count = str_ambient_count;
    }
    
    public void set_result(String str_result){
        this.str_result = str_result;
    }
    
    //Getters////////////////////////////////////////////////////////////////////////////////////////////////
    public String get_market(){
        return str_market_segment;
    }
    
    public String get_family(){
        return str_family;
    }
    
    public String get_current_crosssection(){
        return str_current_crosssection;
    }
    
    public String get_current_crosssection_value(){
        return str_current_crosssection_value;
    }
    
    public String get_sc_mc(){
        return str_sc_mc;
    }
    
    public String get_mc_core_number(){
        return str_mc_core_count;
    }
    
    public String get_frequency(){
        return str_frequency;
    }
    
    public String get_conductor_temperature(){
        return str_conductor_temperature;
    }
    
    public String get_ambient_temperature(){
        return str_ambient_temperature;
    }
    
    public String get_installation_mode(){
        return str_ambient_installation_mode;
    }
    
    public String get_count(){
        return str_ambient_count;
    }
    
    public String get_result(){
        return str_result;
    }
    
    
    public void reset_data(){
        str_market_segment = "choose";
        str_family = "no market has been choosen";
        str_current_crosssection = "choose";
        str_current_crosssection_value = "choose";
        str_sc_mc = "choose";
        str_mc_core_count = "choose";
        str_frequency = "choose";
        str_conductor_temperature = "choose";
        str_ambient_temperature = "choose";
        str_ambient_installation_mode = "choose";
        str_ambient_count = "choose";
        str_result = "choose";
    }
}
