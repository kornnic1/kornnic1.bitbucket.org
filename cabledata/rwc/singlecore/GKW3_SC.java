package cabledata.rwc.singlecore;
import  cabledata.rwc.*;
import  cabledata.awc.*;
import  cabledata.iwc.*;

import javax.swing.*;
import java.util.*;
import java.net.*;

/**
 * This class builds cable objects
 */
public class GKW3_SC{
    //Variables
    //Arrays
    private String[]                arr_str_current_crosssection;   //Calculation mode: calculation with cross section input oder current input
    private String[]                arr_str_crosssection_mm;        //cross section in mm
    private String[]                arr_str_crosssection_awg;       //cross section in awg
    private String[]                arr_str_current_a;              //current in ampere
    private String[]                arr_str_multi_core_count;       //number of cores in the cable (if it's a singlecore, then only 1 core is given by the class)
    private String[]                arr_str_ambient_temperature;    //ambient temperature
    private String[]                arr_str_conductor_temperature;  //conductor temperature
    private String[]                arr_str_frequency;              //frequency of current
    private String[]                arr_str_inst_mode;              //installation mode of the cable; means how and where the cable be used
    private String[]                arr_str_number_of_other_cables; //nummber of cable next lying to the calculated cable
    private String[]                arr_str_nominal_currents;       //nummber of cable next lying to the calculated cable
    
    //Strings
    private String                  str_current_nominal;
    private String                  str_ambient_temp;
    private String                  str_red_fac_amb_temp;
    private String                  str_cond_temp;
    private String                  str_red_fac_cond_temp;
    private String                  str_freq;
    private String                  str_red_fac_freq;
    private String                  str_number_of_other_cables;
    private String                  str_inst_mode;
    private String                  str_red_fac_inst_mode;
    private String                  str_crosssection;
    
    private String                  str_default_current_crossection;
    private String                  str_default_crossection;
    private String                  str_default_multi_core_count;
    private String                  str_default_ambient_temperature;
    private String                  str_default_conductor_temperature;
    private String                  str_default_frequency;
    private String                  str_default_inst_mode;
    private String                  str_default_number_of_other_cables;
    
    /**
     * Constructor for objects of class GKW3_SC
     */
    public GKW3_SC(){
        //init data for dropdown menus in the applet
        arr_str_current_crosssection = new String[]{"Current [A]","Cross Section [mm2]","Cross Section [AWG]"};
        arr_str_crosssection_mm = new String[]{"0.5","0.75","1","1.5","2.5","4","6","10","16","25","35","50","70","95","120","150","185","240","300","400"};
        arr_str_crosssection_awg = new String[]{"dummy"};
        arr_str_current_a = new String[]{"dummy"};
        arr_str_multi_core_count = new String[]{"1"};
        arr_str_ambient_temperature = new String[]{"30","35","40","45","50","55","60","65","70","75","80","85","90","95","100","105","110","115"};
        arr_str_conductor_temperature = new String[]{"80","90","100","110","120"};
        arr_str_frequency = new String[]{"not available"};
        arr_str_inst_mode = new String[]{"air","tray 1 layer","tray 2 layer","2 trays with each 2 layers","tray stappled","on floor or wall","on ceiling","in a pipe"};
        arr_str_number_of_other_cables = new String[]{"1","4","6","8","10","16","20"};
        arr_str_nominal_currents = new String[]{"19","24","29","36","49","66","85","121","163","219","272","344","439","523","621","723","825","996","1150","1473"};
        
        //init default values for the family
        str_default_current_crossection = "Cross Section [mm2]";
        str_default_crossection = "1.5";
        str_default_multi_core_count = "1";
        str_default_ambient_temperature = "30";
        str_default_conductor_temperature = "100";
        str_default_frequency = "not available";
        str_default_inst_mode = "air";
        str_default_number_of_other_cables = "1";
    }

    /**
     * this method loads the available calculations start points with cross section and current
     */    
    public String[] load_current_crosssection(){
        return arr_str_current_crosssection;
    }

    /**
     * this method loads the available cross sections of awg into the dropdownlist
     */    
    public String[] load_crosssections_awg(){
        return arr_str_crosssection_awg;
    }
    
    /**
     * this method loads the available cross sections of mm2 into the dropdownlist
     */    
    public String[] load_crosssections_mm(){
        return arr_str_crosssection_mm;
    }
    
    /**
     * this method loads the available table currents into the dropdownlist
     */    
    public String[] load_current(){
        return arr_str_current_a;
    }
    
    /**
     * this method loads the available table currents into the dropdownlist
     */    
    public String[] load_multi_core_count(){
        return arr_str_multi_core_count;
    }
    
    /**
     * this method loads the possible ambient temperatures into the dropdownlist
     */    
    public String[] load_ambient_temp(){
        return arr_str_ambient_temperature;
    }
    
    /**
     * this method loads the possible conductor temperatures into the dropdownlost
     */    
    public String[] load_conductor_temp(){
        return arr_str_conductor_temperature;
    }    
    
    /**
     * this method loads the possible current frequencies into the dropdownlist
     */    
    public String[] load_frequency(){
        return arr_str_frequency;
    }
    
    /**
     * this method loads the possible installation modes into the dropdownlist
     */    
    public String[] load_installation_mode(){
        return arr_str_inst_mode;
    }    
    
    /**
     * this method loads the possible number of ther cables into the dropdownlist
     */    
    public String[] load_nr_of_other_cables(){
        return arr_str_number_of_other_cables;
    }    
    
    /**
     * this method loads the nominal currents
     */    
    public String[] load_nominal_currents(){
        return arr_str_nominal_currents;
    }    
    
    /**
     * this method gives back the cross section according to the nominal current. CAUTION! Cases needs 1 decimal place e.g. 18.5, 17.0 
     */    
    public String get_crosssection(String str_nominal_current){
        switch(str_nominal_current){
            case "19.0":        str_crosssection = "0.5";
            break;
            case "24.0":        str_crosssection = "0.75";
            break;
            case "29.0":        str_crosssection = "1.0";
            break;
            case "36.0":        str_crosssection = "1.5";
            break;
            case "49.0":        str_crosssection = "2.5";
            break;
            case "66.0":        str_crosssection = "4";
            break;
            case "85.0":        str_crosssection = "6";
            break;
            case "121.0":       str_crosssection = "10";
            break;
            case "163.0":       str_crosssection = "16";
            break;
            case "219.0":       str_crosssection = "25";
            break;
            case "272.0":       str_crosssection = "35";
            break;
            case "344.0":       str_crosssection = "50";
            break;
            case "439.0":       str_crosssection = "70";
            break;
            case "523.0":       str_crosssection = "95";
            break;
            case "621.0":       str_crosssection = "120";
            break;
            case "723.0":       str_crosssection = "150";
            break;
            case "825.0":       str_crosssection = "185";
            break;
            case "996.0":       str_crosssection = "240";
            break;
            case "1150.0":     str_crosssection = "300";
            break;
            case "1473.0":     str_crosssection = "400";
            break;
            default:            str_crosssection = "0";
        }
        return str_crosssection;
    }
    
    /**
     * this method gives back the nominal current for each known conductor cross section str_current_nominal
     */    
    public String get_current_nominal(String str_crosssection){
        switch(str_crosssection){
            case "0.5":         str_current_nominal = "19";
            break;
            case "0.75":        str_current_nominal = "24";
            break;  
            case "1":         str_current_nominal = "29";
            break;
            case "1.5":         str_current_nominal = "36";
            break;
            case "2.5":         str_current_nominal = "49";
            break;
            case "4":         str_current_nominal = "66";
            break;
            case "6":         str_current_nominal = "85";
            break;
            case "10":        str_current_nominal = "121";
            break;
            case "16":        str_current_nominal = "163";
            break;
            case "25":        str_current_nominal = "219";
            break;
            case "35":        str_current_nominal = "272";
            break;
            case "50":        str_current_nominal = "344";
            break;
            case "70":        str_current_nominal = "439";
            break;
            case "95":        str_current_nominal = "523";
            break;
            case "120":       str_current_nominal = "621";
            break;
            case "150":       str_current_nominal = "723";
            break;
            case "185":       str_current_nominal = "825";
            break;
            case "240":       str_current_nominal = "996";
            break;
            case "300":       str_current_nominal = "1150";
            break;
            case "400":       str_current_nominal = "1473";
            break;
            default:            str_current_nominal = "0";
        }
        return str_current_nominal;
    }
    
    /**
     * this method gives back the reductionfactor for ambient temperatures
     */    
    public String get_red_fac_amb_temp(String str_amb_temp){
        switch(str_amb_temp){
            case "30":      str_red_fac_amb_temp = "1";
            break;
            case "35":      str_red_fac_amb_temp = "0.97";
            break;
            case "40":      str_red_fac_amb_temp = "0.94";
            break;
            case "45":      str_red_fac_amb_temp = "0.91";
            break;
            case "50":      str_red_fac_amb_temp = "0.88";
            break;
            case "55":      str_red_fac_amb_temp = "0.85";
            break;
            case "60":      str_red_fac_amb_temp = "0.82";
            break;
            case "65":      str_red_fac_amb_temp = "0.78";
            break;
            case "70":      str_red_fac_amb_temp = "0.75";
            break;
            case "75":      str_red_fac_amb_temp = "0.71";
            break;
            case "80":      str_red_fac_amb_temp = "0.67";
            break;
            case "85":      str_red_fac_amb_temp = "0.62";
            break;
            case "90":      str_red_fac_amb_temp = "0.58";
            break;
            case "95":      str_red_fac_amb_temp = "0.53";
            break;
            case "100":     str_red_fac_amb_temp = "0.47";
            break;
            case "105":     str_red_fac_amb_temp = "0.41";
            break;
            case "110":     str_red_fac_amb_temp = "0.33";
            break;
            case "115":     str_red_fac_amb_temp = "0.22";
            break;
            default:        str_red_fac_amb_temp = "Error";
        }
        return str_red_fac_amb_temp;
    }
    
    /**
     * this method gives back the reductionfactor for conductor temperatures
     */    
    public String get_red_fac_cond_temp(String str_cond_temp){
        switch(str_cond_temp){
            case "80":      str_red_fac_cond_temp = "0.79";
            break;
            case "90":      str_red_fac_cond_temp = "0.85";
            break;
            case "100":     str_red_fac_cond_temp = "0.91";
            break;
            case "110":     str_red_fac_cond_temp = "0.96";
            break;
            case "120":     str_red_fac_cond_temp = "1.0";
            break;
            default:        str_red_fac_cond_temp = "Error";
        }
        return str_red_fac_cond_temp;
    }
    
    /**
     * this method gives back the reductionfactor for frequency: Caution! No frequency reduction available for 3GKW cables, thats why the return value is just 1!
     */    
    public String get_red_fac_freq(String str_freq, String str_crosssection){
        return str_red_fac_freq = "1";
    }
    
    /**
     * this method gives back the reductionfactor for frequency
     */    
    public String get_red_fac_mode(String str_inst_mode, String str_number_of_other_cables){
        switch(str_inst_mode){
            case "air":    
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                default:        str_red_fac_inst_mode = "only 1 cable";
                            }
            break;
            case "tray 1 layer":     
                            switch(str_number_of_other_cables){
                                case "2":       str_red_fac_inst_mode = "0.87";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.81";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.78";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.75";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.74";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.73";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.72";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.71";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "tray 2 layer":    
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.71";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.62";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.57";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.53";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.47";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.45";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "2 trays with each 2 layers":       
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.67";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.59";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.54";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.50";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.45";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.43";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "tray stappled":     
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.71";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.58";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.52";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.48";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.41";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.38";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "on floor or wall":     
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.85";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.79";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.75";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "on ceiling":       
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "0.95";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.81";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.72";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.68";
                                break;
                                case "5":       str_red_fac_inst_mode = "0.66";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.64";
                                break;
                                case "7":       str_red_fac_inst_mode = "0.63";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.62";
                                break;
                                case "� 9":     str_red_fac_inst_mode = "0.61";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "in a pipe":       
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.80";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.70";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.65";
                                break;
                                case "5":       str_red_fac_inst_mode = "0.60";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.57";
                                break;
                                case "7":       str_red_fac_inst_mode = "0.54";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.52";
                                break;
                                case "9":       str_red_fac_inst_mode = "0.50";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.48";
                                break;
                                case "12":      str_red_fac_inst_mode = "0.45";
                                break;
                                case "14":      str_red_fac_inst_mode = "0.43";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.41";
                                break;
                                case "20":     str_red_fac_inst_mode = "0.38";
                                break;
                                default:       str_red_fac_inst_mode = "error";
                            }
            break;
            default:        str_red_fac_inst_mode = "Error";
        }
        return str_red_fac_inst_mode;
    }
    
    /**
     * this method gives back the default value of the calculation mode with current or crosssection as input
     */    
    public String get_default_current_crosssection(){
        return str_default_current_crossection;
    }    
    
    /**
     * this method gives back the default cross section
     */    
    public String get_default_crosssection(){
        return str_default_crossection;
    }    
    
    /**
     * this method gives back the default amount of cores
     */    
    public String get_default_multi_core_count(){
        return str_default_multi_core_count;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_ambient_temperature(){
        return str_default_ambient_temperature;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_conductor_temperature(){
        return str_default_conductor_temperature;
    }    
    
    /**
     * this method gives back the default current frequency
     */    
    public String get_default_frequency(){
        return str_default_frequency;
    }    
    
    /**
     * this method gives back the default installation mode
     */    
    public String get_default_inst_mode(){
        return str_default_inst_mode;
    }   
    
    /**
     * this method gives back the default installation mode
     */    
    public String get_default_number_of_other_cables(){
        return str_default_number_of_other_cables;
    }   
}
