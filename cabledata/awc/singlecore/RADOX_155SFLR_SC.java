package cabledata.awc.singlecore;

import javax.swing.*;
import java.util.*;
import java.net.*;

/**
 * This class builds cable objects
 */
public class RADOX_155SFLR_SC{
    //Variables
    //Arrays
    private String[]                arr_str_current_crosssection;   //Calculation mode: calculation with cross section input oder current input
    private String[]                arr_str_crosssection_mm;        //cross section in mm
    private String[]                arr_str_crosssection_awg;       //cross section in awg
    private String[]                arr_str_current_a;              //current in ampere
    private String[]                arr_str_multi_core_count;       //number of cores in the cable (if it's a singlecore, then only 1 core is given by the class)
    private String[]                arr_str_ambient_temperature;    //ambient temperature
    private String[]                arr_str_conductor_temperature;  //conductor temperature
    private String[]                arr_str_frequency;              //frequency of current
    private String[]                arr_str_inst_mode;              //installation mode of the cable; means how and where the cable be used
    private String[]                arr_str_number_of_other_cables; //nummber of cable next lying to the calculated cable
    private String[]                arr_str_nominal_currents;       //nummber of cable next lying to the calculated cable
    
    //Strings
    private String                  str_current_nominal;
    private String                  str_ambient_temp;
    private String                  str_red_fac_amb_temp;
    private String                  str_cond_temp;
    private String                  str_red_fac_cond_temp;
    private String                  str_freq;
    private String                  str_red_fac_freq;
    private String                  str_number_of_other_cables;
    private String                  str_inst_mode;
    private String                  str_red_fac_inst_mode;
    private String                  str_crosssection;
    
    private String                  str_default_current_crossection;
    private String                  str_default_crossection;
    private String                  str_default_multi_core_count;
    private String                  str_default_ambient_temperature;
    private String                  str_default_conductor_temperature;
    private String                  str_default_frequency;
    private String                  str_default_inst_mode;
    private String                  str_default_number_of_other_cables;
    
    /**
     * Constructor for objects of class RADOX_155SFLR_SC
     */
    public RADOX_155SFLR_SC(){
        //init data for dropdown menus in the applet
        arr_str_current_crosssection = new String[]{"Current [A]","Cross Section [mm2]","Cross Section [AWG]"};
        arr_str_crosssection_mm = new String[]{"0.35","0.5","0.75","1","1.5","2.5","4","6"};
        arr_str_crosssection_awg = new String[]{"dummy"};
        arr_str_current_a = new String[]{"dummy"};
        arr_str_multi_core_count = new String[]{"1"};
        arr_str_ambient_temperature = new String[]{"30","35","40","45","50","55","60","65","70","75","80","85","90","95","100","105","110","115","120","125","130","135","140"};
        arr_str_conductor_temperature = new String[]{"40","50","60","70","80","90","100","110","120","130","140","150","160","170","180"};
        arr_str_frequency = new String[]{"� 200","400","600","800","1000","2000","3000","4000","5000","10000"};
        arr_str_inst_mode = new String[]{"air","tray 1 layer","tray 2 layer","2 trays with each 2 layers","tray stappled","on floor or wall","on ceiling","in a pipe"};
        arr_str_number_of_other_cables = new String[]{"1","4","6","8","10","16","20"};
        arr_str_nominal_currents = new String[]{"18","24","31","37","47","65","88","113"};
        
        //init default values for the family
        str_default_current_crossection = "Cross Section [mm2]";
        str_default_crossection = "1.5";
        str_default_multi_core_count = "1";
        str_default_ambient_temperature = "30";
        str_default_conductor_temperature = "100";
        str_default_frequency = "� 200";
        str_default_inst_mode = "air";
        str_default_number_of_other_cables = "1";
    }

    /**
     * this method loads the available calculations start points with cross section and current
     */    
    public String[] load_current_crosssection(){
        return arr_str_current_crosssection;
    }

    /**
     * this method loads the available cross sections of awg into the dropdownlist
     */    
    public String[] load_crosssections_awg(){
        return arr_str_crosssection_awg;
    }
    
    /**
     * this method loads the available cross sections of mm2 into the dropdownlist
     */    
    public String[] load_crosssections_mm(){
        return arr_str_crosssection_mm;
    }
    
    /**
     * this method loads the available table currents into the dropdownlist
     */    
    public String[] load_current(){
        return arr_str_current_a;
    }
    
    /**
     * this method loads the available table currents into the dropdownlist
     */    
    public String[] load_multi_core_count(){
        return arr_str_multi_core_count;
    }
    
    /**
     * this method loads the possible ambient temperatures into the dropdownlist
     */    
    public String[] load_ambient_temp(){
        return arr_str_ambient_temperature;
    }
    
    /**
     * this method loads the possible conductor temperatures into the dropdownlost
     */    
    public String[] load_conductor_temp(){
        return arr_str_conductor_temperature;
    }    
    
    /**
     * this method loads the possible current frequencies into the dropdownlist
     */    
    public String[] load_frequency(){
        return arr_str_frequency;
    }
    
    /**
     * this method loads the possible installation modes into the dropdownlist
     */    
    public String[] load_installation_mode(){
        return arr_str_inst_mode;
    }    
    
    /**
     * this method loads the possible number of ther cables into the dropdownlist
     */    
    public String[] load_nr_of_other_cables(){
        return arr_str_number_of_other_cables;
    }    
    
    /**
     * this method loads the nominal currents
     */    
    public String[] load_nominal_currents(){
        return arr_str_nominal_currents;
    }    
    
    /**
     * this method gives back the cross section according to the nominal current. CAUTION! Cases needs 1 decimal place e.g. 18.5, 17.0 
     */    
    public String get_crosssection(String str_nominal_current){
        switch(str_nominal_current){
            case "18.0":    str_crosssection = "0.35";
            break;
            case "24.0":      str_crosssection = "0.5";
            break;
            case "31.0":      str_crosssection = "0.75";
            break;
            case "37.0":      str_crosssection = "1";
            break;
            case "47.0":      str_crosssection = "1.5";
            break;
            case "65.0":      str_crosssection = "2.5";
            break;
            case "88.0":      str_crosssection = "4";
            break;
            case "113.0":     str_crosssection = "6";
            break;
            default:        str_crosssection = "0";
        }
        return str_crosssection;
    }
    
    /**
     * this method gives back the nominal current for each known conductor cross section
     */    
    public String get_current_nominal(String str_crosssection){
        switch(str_crosssection){
            case "0.35":    str_current_nominal = "18";
            break;
            case "0.5":     str_current_nominal = "24";
            break;
            case "0.75":    str_current_nominal = "31";
            break;
            case "1":       str_current_nominal = "37";
            break;
            case "1.5":     str_current_nominal = "47";
            break;
            case "2.5":     str_current_nominal = "65";
            break;
            case "4":       str_current_nominal = "88";
            break;
            case "6":       str_current_nominal = "113";
            break;
            default:        str_current_nominal = "0";
        }
        return str_current_nominal;
    }
    
    /**
     * this method gives back the reductionfactor for ambient temperatures
     */    
    public String get_red_fac_amb_temp(String str_amb_temp){
        switch(str_amb_temp){
            case "30":      str_red_fac_amb_temp = "1";
            break;
            case "35":      str_red_fac_amb_temp = "0.97";
            break;
            case "40":      str_red_fac_amb_temp = "0.95";
            break;
            case "45":      str_red_fac_amb_temp = "0.93";
            break;
            case "50":      str_red_fac_amb_temp = "0.91";
            break;
            case "55":      str_red_fac_amb_temp = "0.89";
            break;
            case "60":      str_red_fac_amb_temp = "0.86";
            break;
            case "65":      str_red_fac_amb_temp = "0.84";
            break;
            case "70":      str_red_fac_amb_temp = "0.81";
            break;
            case "75":      str_red_fac_amb_temp = "0.79";
            break;
            case "80":      str_red_fac_amb_temp = "0.76";
            break;
            case "85":      str_red_fac_amb_temp = "0.73";
            break;
            case "90":      str_red_fac_amb_temp = "0.7";
            break;
            case "95":      str_red_fac_amb_temp = "0.67";
            break;
            case "100":     str_red_fac_amb_temp = "0.64";
            break;
            case "105":     str_red_fac_amb_temp = "0.61";
            break;
            case "110":     str_red_fac_amb_temp = "0.57";
            break;
            case "115":     str_red_fac_amb_temp = "0.54";
            break;
            case "120":     str_red_fac_amb_temp = "0.50";
            break;
            case "125":     str_red_fac_amb_temp = "0.45";
            break;
            case "130":     str_red_fac_amb_temp = "0.40";
            break;
            case "135":     str_red_fac_amb_temp = "0.35";
            break;
            case "140":     str_red_fac_amb_temp = "0.28";
            break;
            case "145":     str_red_fac_amb_temp = "0.20";
            break;
            default:        str_red_fac_amb_temp = "Error";
        }
        return str_red_fac_amb_temp;
    }
    
    /**
     * this method gives back the reductionfactor for conductor temperatures
     */    
    public String get_red_fac_cond_temp(String str_cond_temp){
        switch(str_cond_temp){
            case "40":      str_red_fac_cond_temp = "0.16";
            break;
            case "50":      str_red_fac_cond_temp = "0.27";
            break;
            case "60":      str_red_fac_cond_temp = "0.37";
            break;
            case "70":      str_red_fac_cond_temp = "0.47";
            break;
            case "80":      str_red_fac_cond_temp = "0.56";
            break;
            case "90":      str_red_fac_cond_temp = "0.67";
            break;
            case "100":     str_red_fac_cond_temp = "0.72";
            break;
            case "110":     str_red_fac_cond_temp = "0.79";
            break;
            case "120":     str_red_fac_cond_temp = "0.85";
            break;
            case "130":     str_red_fac_cond_temp = "0.91";
            break;
            case "140":     str_red_fac_cond_temp = "0.96";
            break;
            case "150":     str_red_fac_cond_temp = "1";
            break;
            case "160":     str_red_fac_cond_temp = "1.04";
            break;
            case "170":     str_red_fac_cond_temp = "1.07";
            break;
            case "180":     str_red_fac_cond_temp = "1.09";
            break;
            default:        str_red_fac_cond_temp = "Error";
        }
        return str_red_fac_cond_temp;
    }
    
    /**
     * this method gives back the reductionfactor for frequency
     */    
    public String get_red_fac_freq(String str_freq, String str_crosssection){
        switch(str_crosssection){
            case "0.35":    switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "0.5":     switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "0.75":    switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "1":       switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "1.5":     switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "2.5":     switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "1";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "4":       switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "1";
                                break;
                                case "10000":   str_red_fac_freq = "0.98";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            case "6":       switch(str_freq){
                                case "� 200":   str_red_fac_freq = "1";
                                break;
                                case "400":     str_red_fac_freq = "1";
                                break;
                                case "600":     str_red_fac_freq = "1";
                                break;
                                case "800":     str_red_fac_freq = "1";
                                break;
                                case "1000":    str_red_fac_freq = "1";
                                break;
                                case "2000":    str_red_fac_freq = "1";
                                break;
                                case "3000":    str_red_fac_freq = "1";
                                break;
                                case "4000":    str_red_fac_freq = "1";
                                break;
                                case "5000":    str_red_fac_freq = "0.99";
                                break;
                                case "10000":   str_red_fac_freq = "0.93";
                                break;
                                default:        str_red_fac_freq = "error";
                            }
            break;
            default:        str_red_fac_freq = "error";
        }
        return str_red_fac_freq;
    }
    
    /**
     * this method gives back the reductionfactor for frequency
     */    
    public String get_red_fac_mode(String str_inst_mode, String str_number_of_other_cables){
        switch(str_inst_mode){
            case "air":    
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                default:        str_red_fac_inst_mode = "only 1 cable";
                            }
            break;
            case "tray 1 layer":     
                            switch(str_number_of_other_cables){
                                case "2":       str_red_fac_inst_mode = "0.87";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.81";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.78";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.75";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.74";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.73";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.72";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.71";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "tray 2 layer":    
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.71";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.62";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.57";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.53";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.47";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.45";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "2 trays with each 2 layers":       
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.67";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.59";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.54";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.50";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.45";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.43";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "tray stappled":     
                            switch(str_number_of_other_cables){
                                case "4":       str_red_fac_inst_mode = "0.71";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.58";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.52";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.48";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.41";
                                break;
                                case "20":      str_red_fac_inst_mode = "0.38";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "on floor or wall":     
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.85";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.79";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.75";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "on ceiling":       
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "0.95";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.81";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.72";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.68";
                                break;
                                case "5":       str_red_fac_inst_mode = "0.66";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.64";
                                break;
                                case "7":       str_red_fac_inst_mode = "0.63";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.62";
                                break;
                                case "� 9":     str_red_fac_inst_mode = "0.61";
                                break;
                                default:        str_red_fac_inst_mode = "error";
                            }
            break;
            case "in a pipe":       
                            switch(str_number_of_other_cables){
                                case "1":       str_red_fac_inst_mode = "1";
                                break;
                                case "2":       str_red_fac_inst_mode = "0.80";
                                break;
                                case "3":       str_red_fac_inst_mode = "0.70";
                                break;
                                case "4":       str_red_fac_inst_mode = "0.65";
                                break;
                                case "5":       str_red_fac_inst_mode = "0.60";
                                break;
                                case "6":       str_red_fac_inst_mode = "0.57";
                                break;
                                case "7":       str_red_fac_inst_mode = "0.54";
                                break;
                                case "8":       str_red_fac_inst_mode = "0.52";
                                break;
                                case "9":       str_red_fac_inst_mode = "0.50";
                                break;
                                case "10":      str_red_fac_inst_mode = "0.48";
                                break;
                                case "12":      str_red_fac_inst_mode = "0.45";
                                break;
                                case "14":      str_red_fac_inst_mode = "0.43";
                                break;
                                case "16":      str_red_fac_inst_mode = "0.41";
                                break;
                                case "20":     str_red_fac_inst_mode = "0.38";
                                break;
                                default:       str_red_fac_inst_mode = "error";
                            }
            break;
            default:        str_red_fac_inst_mode = "Error";
        }
        return str_red_fac_inst_mode;
    }
    
    /**
     * this method gives back the default value of the calculation mode with current or crosssection as input
     */    
    public String get_default_current_crosssection(){
        return str_default_current_crossection;
    }    
    
    /**
     * this method gives back the default cross section
     */    
    public String get_default_crosssection(){
        return str_default_crossection;
    }    
    
    /**
     * this method gives back the default amount of cores
     */    
    public String get_default_multi_core_count(){
        return str_default_multi_core_count;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_ambient_temperature(){
        return str_default_ambient_temperature;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_conductor_temperature(){
        return str_default_conductor_temperature;
    }    
    
    /**
     * this method gives back the default current frequency
     */    
    public String get_default_frequency(){
        return str_default_frequency;
    }    
    
    /**
     * this method gives back the default installation mode
     */    
    public String get_default_inst_mode(){
        return str_default_inst_mode;
    }   
    
    /**
     * this method gives back the default installation mode
     */    
    public String get_default_number_of_other_cables(){
        return str_default_number_of_other_cables;
    }   
}
