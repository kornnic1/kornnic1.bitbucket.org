package cabledata.awc;
import cabledata.awc.singlecore.*;


import javax.swing.*;
import java.util.*;
import java.net.*;

/**
 * This class builds cable objects
 */
public class AWC_Cables{
    //Variables
    //Arrays
    private String[]                arr_str_cable_families;
    private String[]                arr_str_sc_mc;
    private String[]                arr_str_current_crosssection;
    private String[]                arr_str_crosssection_mm;
    private String[]                arr_str_crosssection_awg; 
    private String[]                arr_str_current_a;
    private String[]                arr_str_multi_core_count;
    private String[]                arr_str_ambient_temperature;
    private String[]                arr_str_conductor_temperature;
    private String[]                arr_str_frequency;
    private String[]                arr_str_inst_mode;
    private String[]                arr_str_number_of_other_cables;
    private String[]                arr_str_nominal_currents;
    
    //Strings
    private String                  str_sc_mc;
    private String                  str_cable_family;
    private Boolean                 bol_multicore_available;
    
    private String                  str_default_current_crossection;
    private String                  str_default_crossection;
    private String                  str_default_multi_core_count;
    private String                  str_default_ambient_temperature;
    private String                  str_default_conductor_temperature;
    private String                  str_default_frequency;
    private String                  str_default_inst_mode;
    private String                  str_default_number_of_other_cables;
    
    private String                  str_nominal_current;
    private String                  str_factor_core_number;
    private String                  str_factor_conductor_temperature;
    private String                  str_factor_ambient_temperature;
    private String                  str_factor_frequency;
    private String                  str_factor_installation_mode;
    private String                  str_crosssection;

    //all cable families of the automotive market segment
    private RADOX_155SFLR_SC radox_155sflr_sc;
    private RADOX_155_REMS_SC radox_155_rems_sc;
    
    /**
     * Constructor for objects of class AWC_Cables
     */
    public AWC_Cables(){
        arr_str_cable_families = new String[]{"choose","RADOX 155S FLR","RADOX 155/REMS"};
    }

    /**
     * gives back all available cable families in the market segment of automotive
     */
    public String[] load_awc(){
        return arr_str_cable_families;
    }

    public void init_cable_family(String str_cable_family, String str_sc_mc){
        this.str_cable_family = str_cable_family;
        this.str_sc_mc = str_sc_mc;
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      radox_155sflr_sc = new RADOX_155SFLR_SC();
                                            //load default setup
                break;
                case "RADOX 155/REMS":      radox_155_rems_sc = new RADOX_155_REMS_SC();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                //case "RADOX 155S FLR":      radox_155sflr_mc = new RADOX_155SFLR_MC();
                //break;
                //case "RADOX 155/REMS":      radox_155_rems_mc = new RADOX_155_REMS_MC();
                //break;
                default:;
            }
        }
    }
    
    public String[] load_sc_mc(String str_cable_family){
        switch(str_cable_family){
            case "RADOX 155S FLR":      arr_str_sc_mc = new String[]{"choose","Singlecore"};
            break;
            case "RADOX 155/REMS":      arr_str_sc_mc = new String[]{"choose","Singlecore"};
            break;
            default:;
        }
        return arr_str_sc_mc;
    }
    
    public String[] load_current_crosssection(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_current_crosssection = radox_155sflr_sc.load_current_crosssection();
                break;
                case "RADOX 155/REMS":      arr_str_current_crosssection = radox_155_rems_sc.load_current_crosssection();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_current_crosssection = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_current_crosssection = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_current_crosssection;
    }
    
    public String[] load_crosssections_mm(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_crosssection_mm = radox_155sflr_sc.load_crosssections_mm();
                break;
                case "RADOX 155/REMS":      arr_str_crosssection_mm = radox_155_rems_sc.load_crosssections_mm();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_crosssection_mm = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_crosssection_mm = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_crosssection_mm;
    }
    
    public String[] load_crosssections_awg(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_crosssection_awg = radox_155sflr_sc.load_crosssections_awg();
                break;
                case "RADOX 155/REMS":      arr_str_crosssection_awg = radox_155_rems_sc.load_crosssections_awg();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_crosssection_awg = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_crosssection_awg = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_crosssection_awg;
    }
    
    public String[] load_current(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_current_a = radox_155sflr_sc.load_current();
                break;
                case "RADOX 155/REMS":      arr_str_current_a = radox_155_rems_sc.load_current();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_current_a = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_current_a = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_current_a;
    }
    
    public String[] load_mc_core_number(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_multi_core_count = radox_155sflr_sc.load_multi_core_count();
                break;
                case "RADOX 155/REMS":      arr_str_multi_core_count = radox_155_rems_sc.load_multi_core_count();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_multi_core_count = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_multi_core_count = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_multi_core_count;
    }
    
    public String[] load_ambient_temp(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_ambient_temperature = radox_155sflr_sc.load_ambient_temp();
                break;
                case "RADOX 155/REMS":      arr_str_ambient_temperature = radox_155_rems_sc.load_ambient_temp();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_ambient_temperature = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_ambient_temperature = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_ambient_temperature;
    }
    
    public String[] load_conductor_temp(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_conductor_temperature = radox_155sflr_sc.load_conductor_temp();
                break;
                case "RADOX 155/REMS":      arr_str_conductor_temperature = radox_155_rems_sc.load_conductor_temp();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_conductor_temperature = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_conductor_temperature = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_conductor_temperature;
    }
    
    public String[] load_frequency(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":          arr_str_frequency = radox_155sflr_sc.load_frequency();
                break;
                case "RADOX 155/REMS":          arr_str_frequency = radox_155_rems_sc.load_frequency();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_frequency = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_frequency = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_frequency;
    }
    
    public String[] load_installation_mode(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_inst_mode = radox_155sflr_sc.load_installation_mode();
                break;
                case "RADOX 155/REMS":      arr_str_inst_mode = radox_155_rems_sc.load_installation_mode();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_inst_mode = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_inst_mode = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_inst_mode;
    }
    
    public String[] load_nr_of_other_cables(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_number_of_other_cables = radox_155sflr_sc.load_nr_of_other_cables();
                break;
                case "RADOX 155/REMS":      arr_str_number_of_other_cables = radox_155_rems_sc.load_nr_of_other_cables();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_number_of_other_cables = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_number_of_other_cables = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_number_of_other_cables;
    }
    
    public String[] load_nominal_currents(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_nominal_currents = radox_155sflr_sc.load_nominal_currents();
                break;
                case "RADOX 155/REMS":      arr_str_nominal_currents = radox_155_rems_sc.load_nominal_currents();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      arr_str_nominal_currents = new String[]{"n.a. for multicore cables"};
                break;
                case "RADOX 155/REMS":      arr_str_nominal_currents = new String[]{"n.a. for multicore cables"};
                break;
                default:;
            }
        }
        return arr_str_nominal_currents;
    }
    
    /**
     * this method gives back the default value of the calculation mode with current or crosssection as input
     */    
    public String get_default_current_crosssection(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_current_crossection = radox_155sflr_sc.get_default_current_crosssection();
                break;
                case "RADOX 155/REMS":      str_default_current_crossection = radox_155_rems_sc.get_default_current_crosssection();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_current_crossection = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_current_crossection = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_current_crossection;
    }    
    
    /**
     * this method gives back the default cross section
     */    
    public String get_default_crosssection(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_crossection = radox_155sflr_sc.get_default_crosssection();
                break;
                case "RADOX 155/REMS":      str_default_crossection = radox_155_rems_sc.get_default_crosssection();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_crossection = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_crossection = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_crossection;
    }    
    
    /**
     * this method gives back the default amount of cores
     */    
    public String get_default_mc_core_number(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_multi_core_count = radox_155sflr_sc.get_default_multi_core_count();
                break;
                case "RADOX 155/REMS":      str_default_multi_core_count = radox_155_rems_sc.get_default_multi_core_count();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_multi_core_count = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_multi_core_count = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_multi_core_count;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_ambient_temperature(){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_ambient_temperature = radox_155sflr_sc.get_default_ambient_temperature();
                break;
                case "RADOX 155/REMS":      str_default_ambient_temperature = radox_155_rems_sc.get_default_ambient_temperature();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_ambient_temperature = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_ambient_temperature = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_ambient_temperature;
    }    
    
    /**
     * this method gives back the default ambient temperature
     */    
    public String get_default_conductor_temperature(){
          if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_conductor_temperature = radox_155sflr_sc.get_default_conductor_temperature();
                break;
                case "RADOX 155/REMS":      str_default_conductor_temperature = radox_155_rems_sc.get_default_conductor_temperature();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_conductor_temperature = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_conductor_temperature = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_conductor_temperature;
    }    
    
    /**
     * this method gives back the default current frequency
     */    
    public String get_default_frequency(){
          if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_frequency = radox_155sflr_sc.get_default_frequency();
                break;
                case "RADOX 155/REMS":      str_default_frequency = radox_155_rems_sc.get_default_frequency();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_frequency = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_frequency = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_frequency;
    }    
    
    /**
     * this method gives back the default installation mode
     */    
    public String get_default_inst_mode(){
          if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_inst_mode = radox_155sflr_sc.get_default_inst_mode();
                break;
                case "RADOX 155/REMS":      str_default_inst_mode = radox_155_rems_sc.get_default_inst_mode();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_inst_mode = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_inst_mode = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_inst_mode;
    }   
    
    /**
     * this method gives back the total number of cables in the choosen installation mode
     */    
    public String get_default_number_of_other_cables(){
          if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_number_of_other_cables = radox_155sflr_sc.get_default_number_of_other_cables();
                break;
                case "RADOX 155/REMS":      str_default_number_of_other_cables = radox_155_rems_sc.get_default_number_of_other_cables();
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_default_number_of_other_cables = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_default_number_of_other_cables = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_default_number_of_other_cables;
    }   
    
    /**
     * this method gives back the nominal current
     */    
    public String get_current_nominal(String str_crosssection){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_nominal_current = radox_155sflr_sc.get_current_nominal(str_crosssection);
                break;
                case "RADOX 155/REMS":      str_nominal_current = radox_155_rems_sc.get_current_nominal(str_crosssection);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_nominal_current = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_nominal_current = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_nominal_current;
    }    
    
    /**
     * this method gives back the nominal current
     */    
    public String get_crosssection(String str_current_nominal){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_crosssection = radox_155sflr_sc.get_crosssection(str_current_nominal);
                break;
                case "RADOX 155/REMS":      str_crosssection = radox_155_rems_sc.get_crosssection(str_current_nominal);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_crosssection = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_crosssection = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_crosssection;
    }    
    
    /**
     * this method gives back the reductionfactor for the amount of cores in the cable
     */    
    public String get_factor_core_number(String str_core_number){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_core_number = "1";
                break;
                case "RADOX 155/REMS":      str_factor_core_number = "1";
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_core_number = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_factor_core_number = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_factor_core_number;
    }   
    
    /**
     * this method gives back the reductionfactor for conductor temperature
     */    
    public String get_factor_conductor_temperature(String str_conductor_temperature){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_conductor_temperature = radox_155sflr_sc.get_red_fac_cond_temp(str_conductor_temperature);
                break;
                case "RADOX 155/REMS":      str_factor_conductor_temperature = radox_155_rems_sc.get_red_fac_cond_temp(str_conductor_temperature);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_conductor_temperature = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_factor_conductor_temperature = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_factor_conductor_temperature;
    }    
    
    /**
     * this method gives back the reductionfactor for ambient temperature
     */    
    public String get_factor_ambient_temperature(String str_ambient_temperature){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_ambient_temperature = radox_155sflr_sc.get_red_fac_amb_temp(str_ambient_temperature);
                break;
                case "RADOX 155/REMS":      str_factor_ambient_temperature = radox_155_rems_sc.get_red_fac_amb_temp(str_ambient_temperature);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_ambient_temperature = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_factor_ambient_temperature = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_factor_ambient_temperature;
    }    
    
    /**
     * this method gives back the reductionfactor for frequency
     */    
    public String get_factor_frequency(String str_frequency, String str_crosssection){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_frequency = radox_155sflr_sc.get_red_fac_freq(str_frequency,str_crosssection);
                break;
                case "RADOX 155/REMS":      str_factor_frequency = radox_155_rems_sc.get_red_fac_freq(str_frequency,str_crosssection);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_frequency = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_factor_frequency = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_factor_frequency;
    }    
    
    /**
     * this method gives back the reductionfactor for installation mode
     */    
    public String get_factor_installation_mode(String str_installation_mode, String str_number_of_other_cables){
        if(str_sc_mc == "Singlecore"){
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_installation_mode = radox_155sflr_sc.get_red_fac_mode(str_installation_mode,str_number_of_other_cables);
                break;
                case "RADOX 155/REMS":      str_factor_installation_mode = radox_155_rems_sc.get_red_fac_mode(str_installation_mode,str_number_of_other_cables);
                break;
                default:;
            }
        }else{
            switch(str_cable_family){
                case "RADOX 155S FLR":      str_factor_installation_mode = "n.a. for multicore cables";
                break;
                case "RADOX 155/REMS":      str_factor_installation_mode = "n.a. for multicore cables";
                break;
                default:;
            }
        }
        return str_factor_installation_mode;
    }    
    
    public RADOX_155SFLR_SC load_radox_155sflr_sc(){
        return radox_155sflr_sc;
    } 
}
