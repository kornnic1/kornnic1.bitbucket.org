import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.awt.Image;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
class ImageButton{
    private static int IMG_WIDTH = 80;
    private static Color SHAPE_COLOR = Color.RED;
    private static int GAP = 4;
    private static int x;
    private static int y;
    private static int width;
    private static int height;
    private static int i_cable_base_x;
    private static int i_cable_base_y;
   
    //Icons
    private static ImageIcon icon_tray_1l;
    private static ImageIcon icon_tray_2l;
    private static ImageIcon icon_tray_2l_2t;
    private static ImageIcon icon_tray_4l;
    private static ImageIcon icon_air;
    private static ImageIcon icon_ceiling;
    private static ImageIcon icon_floor_wall;
    private static ImageIcon icon_pipe;
    
    //buffered images
    private static BufferedImage image_tray_1l;
    private static BufferedImage image_tray_2l;
    private static BufferedImage image_tray_2l_2t;
    private static BufferedImage image_tray_4l;
    private static BufferedImage image_air;
    private static BufferedImage image_ceiling;
    private static BufferedImage image_floor_wall;
    private static BufferedImage image_pipe;
    
    private static Graphics2D g2;

   
    public ImageButton() {
        x = GAP;
        y = x;
        width = IMG_WIDTH - 2 * x;
        height = IMG_WIDTH - 2 * y;
    }

    public ImageIcon icon_tray_1l() {
        image_tray_1l = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_tray_1l.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        //g2.drawRect(0, 0, IMG_WIDTH-1, IMG_WIDTH-1);
        draw_tray_1l();
        g2.dispose();
        icon_tray_1l = new ImageIcon(image_tray_1l);
        return icon_tray_1l;
    }
   
    public ImageIcon icon_tray_2l() {
        image_tray_2l = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_tray_2l.createGraphics();
        g2 = image_tray_2l.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_tray_2l();
        g2.dispose();
        icon_tray_2l = new ImageIcon(image_tray_2l);
        return icon_tray_2l;
    }
   
    public ImageIcon icon_tray_2l_2t() {
        image_tray_2l_2t = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_tray_2l_2t.createGraphics();
        g2 = image_tray_2l_2t.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_tray_2l_2t();
        g2.dispose();
        icon_tray_2l_2t = new ImageIcon(image_tray_2l_2t);
        return icon_tray_2l_2t;
    }
   
    public ImageIcon icon_tray_4l() {
        image_tray_4l = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_tray_4l.createGraphics();
        g2 = image_tray_4l.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_tray_4l();
        g2.dispose();
        icon_tray_4l = new ImageIcon(image_tray_4l);
        return icon_tray_4l;
    }
   
    public ImageIcon icon_air() {
        image_air = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_air.createGraphics();
        g2 = image_air.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_air();
        g2.dispose();
        icon_air = new ImageIcon(image_air);
        return icon_air;
    }
   
    public ImageIcon icon_ceiling() {
        image_ceiling = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_ceiling.createGraphics();
        g2 = image_ceiling.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_ceiling();
        g2.dispose();
        icon_ceiling = new ImageIcon(image_ceiling);
        return icon_ceiling;
    }
   
    public ImageIcon icon_floor_wall() {
        image_floor_wall = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_floor_wall.createGraphics();
        g2 = image_floor_wall.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_floor_wall();
        g2.dispose();
        icon_floor_wall = new ImageIcon(image_floor_wall);
        return icon_floor_wall;
    }
   
    public ImageIcon icon_pipe() {
        image_pipe = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = image_pipe.createGraphics();
        g2 = image_pipe.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        draw_pipe();
        g2.dispose();
        icon_pipe = new ImageIcon(image_pipe);
        return icon_pipe;
    }
    
    
    //drawings
    private void draw_tray_1l(){
        //Bottom tray
        g2.fillRect(7, 60, 5, 2);
        g2.fillRect(17, 60, 5, 2);
        g2.fillRect(27, 60, 5, 2);
        g2.fillRect(37, 60, 5, 2);
        g2.fillRect(47, 60, 5, 2);
        g2.fillRect(57, 60, 5, 2);
        g2.fillRect(67, 60, 7, 2);
        //Side tray
        g2.fillRect(5, 52, 2, 10);
        g2.fillRect(73, 52, 2, 10);
        //Cables
        i_cable_base_x = 9;
        i_cable_base_y = 51;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+18, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+27, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+36, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+45, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+54, i_cable_base_y, 9, 9);
    }
    
    private void draw_tray_2l(){
        //Bottom tray
        g2.fillRect(7, 60, 5, 2);
        g2.fillRect(17, 60, 5, 2);
        g2.fillRect(27, 60, 5, 2);
        g2.fillRect(37, 60, 5, 2);
        g2.fillRect(47, 60, 5, 2);
        g2.fillRect(57, 60, 5, 2);
        g2.fillRect(67, 60, 7, 2);
        //Side tray
        g2.fillRect(5, 52, 2, 10);
        g2.fillRect(73, 52, 2, 10);
        //Cables
        i_cable_base_x = 9;
        i_cable_base_y = 51;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+18, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+27, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+36, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+45, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+54, i_cable_base_y, 9, 9);
        //Cables second layer
        i_cable_base_x = 9;
        i_cable_base_y = 43;
        g2.fillOval(i_cable_base_x+5, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+14, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+23, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+32, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+41, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+50, i_cable_base_y, 9, 9);
    }
    
    private void draw_tray_2l_2t(){
        //Bottom tray
        g2.fillRect(7, 60, 5, 2);
        g2.fillRect(17, 60, 5, 2);
        g2.fillRect(27, 60, 5, 2);
        g2.fillRect(37, 60, 5, 2);
        g2.fillRect(47, 60, 5, 2);
        g2.fillRect(57, 60, 5, 2);
        g2.fillRect(67, 60, 7, 2);
        //Side tray
        g2.fillRect(5, 52, 2, 10);
        g2.fillRect(73, 52, 2, 10);
        //Cables
        i_cable_base_x = 9;
        i_cable_base_y = 51;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+18, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+27, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+36, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+45, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+54, i_cable_base_y, 9, 9);
        //Cables second layer
        i_cable_base_x = 9;
        i_cable_base_y = 43;
        g2.fillOval(i_cable_base_x+5, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+14, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+23, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+32, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+41, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+50, i_cable_base_y, 9, 9);
        //Bottom tray
        g2.fillRect(7, 35, 5, 2);
        g2.fillRect(17, 35, 5, 2);
        g2.fillRect(27, 35, 5, 2);
        g2.fillRect(37, 35, 5, 2);
        g2.fillRect(47, 35, 5, 2);
        g2.fillRect(57, 35, 5, 2);
        g2.fillRect(67, 35, 7, 2);
        //Side tray
        g2.fillRect(5, 27, 2, 10);
        g2.fillRect(73, 27, 2, 10);
        //Cables
        i_cable_base_x = 9;
        i_cable_base_y = 26;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+18, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+27, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+36, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+45, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+54, i_cable_base_y, 9, 9);
        //Cables second layer
        i_cable_base_x = 9;
        i_cable_base_y = 18;
        g2.fillOval(i_cable_base_x+5, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+14, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+23, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+32, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+41, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+50, i_cable_base_y, 9, 9);
    }
    
    private void draw_tray_4l(){
        //Bottom tray
        g2.fillRect(7, 60, 5, 2);
        g2.fillRect(17, 60, 5, 2);
        g2.fillRect(27, 60, 5, 2);
        g2.fillRect(37, 60, 5, 2);
        g2.fillRect(47, 60, 5, 2);
        g2.fillRect(57, 60, 5, 2);
        g2.fillRect(67, 60, 7, 2);
        //Side tray
        g2.fillRect(5, 52, 2, 10);
        g2.fillRect(73, 52, 2, 10);
        //Cables
        i_cable_base_x = 9;
        i_cable_base_y = 51;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+18, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+27, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+36, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+45, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+54, i_cable_base_y, 9, 9);
        //Cables second layer
        i_cable_base_x = 9;
        i_cable_base_y = 43;
        g2.fillOval(i_cable_base_x+5, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+14, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+23, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+32, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+41, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+50, i_cable_base_y, 9, 9);
        //Cables third layer
        i_cable_base_x = 9;
        i_cable_base_y = 35;
        g2.fillOval(i_cable_base_x+10, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+19, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+28, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+37, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+46, i_cable_base_y, 9, 9);
        //Cables fourth layer
        i_cable_base_x = 9;
        i_cable_base_y = 27;
        g2.fillOval(i_cable_base_x+15, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+24, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+33, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+42, i_cable_base_y, 9, 9);
    }
    
    private void draw_air(){
        //Wall
        g2.fillRect(30, 0, 4, 80);
        g2.drawLine(30, 10, 15, 25);
        g2.drawLine(30, 35, 15, 50);
        g2.drawLine(30, 60, 15, 75);
        //Cables
        i_cable_base_x = 50;
        i_cable_base_y = 35;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
    }
    
    private void draw_ceiling(){
        //Wall
        g2.fillRect(0, 30, 80, 4);
        g2.drawLine(10, 30, 25, 15);
        g2.drawLine(35, 30, 50, 15);
        g2.drawLine(60, 30, 75, 15);
        //Cables
        i_cable_base_x = 35;
        i_cable_base_y = 34;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
    }
    
    private void draw_floor_wall(){
        //Wall
        g2.fillRect(15, 10, 4, 70);
        g2.drawLine(15, 10, 0, 25);
        g2.drawLine(15, 35, 0, 50);
        g2.drawLine(15, 60, 0, 75);
        //Cables
        i_cable_base_x = 19;
        i_cable_base_y = 35;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        
        //Wall
        g2.fillRect(40, 60, 50, 4);
        g2.drawLine(38, 75, 53, 60);
        g2.drawLine(63, 75, 78, 60);
        //Cables
        i_cable_base_x = 55;
        i_cable_base_y = 51;
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
    }
    
    private void draw_pipe(){
        //Wall
        g2.fillRect(30, 0, 4, 80);
        g2.drawLine(30, 10, 15, 25);
        g2.drawLine(30, 35, 15, 50);
        g2.drawLine(30, 60, 15, 75);
        //Cables
        i_cable_base_x = 41;
        i_cable_base_y = 45;
        g2.fillOval(i_cable_base_x+9, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x, i_cable_base_y, 9, 9);
        g2.fillOval(i_cable_base_x+4, i_cable_base_y-7, 9, 9);
        //pipe
        g2.drawOval(34, 25, 30, 30);
    }
}