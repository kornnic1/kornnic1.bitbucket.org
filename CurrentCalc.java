// This applet helps to calculate the permissible current for Huber+Suhner Cable and core
 
import cabledata.awc.*; 
//import cabledata.awc.singlecore.*; 
import cabledata.iwc.*; 
//import cabledata.iwc.singlecore.*; 
import cabledata.rwc.*; 
//import cabledata.rwc.singlecore.*; 
import javax.swing.*;    // Swing GUI classes are defined here.
import java.awt.event.*; // Event handling class are defined here.
import java.awt.*;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.util.*;
import java.util.Collections; 
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import com.seaglasslookandfeel.*;
import javax.swing.UIManager.*;

public class CurrentCalc extends JApplet implements ActionListener, DocumentListener, ItemListener{
    //Panels
    private static JTabbedPane      tp_main;
    private static JPanel_Right     jpr_step_market;
    private static String           str_tab_text_market;
    private static JPanel_Right     jpr_step_family;
    private static String           str_tab_text_family;
    private static JPanel_Right     jpr_step_current_crosssection;
    private static String           str_tab_text_current_crosssection;
    private static JPanel_Right     jpr_step_sc_mc;
    private static String           str_tab_text_sc_mc;
    private static JPanel_Right     jpr_step_mc_core_number;
    private static String           str_tab_text_mc_core_number;
    private static JPanel_Right     jpr_step_frequency;
    private static String           str_tab_text_frequency;
    private static JPanel_Right     jpr_step_conductor_temperature;
    private static String           str_tab_text_conductor_temperature;
    private static JPanel_Right     jpr_step_ambient_temperature;
    private static String           str_tab_text_ambient_temperature;
    private static JPanel_Right     jpr_step_installation_mode;
    private static String           str_tab_text_installation_mode;
    private static JPanel_Right     jpr_step_count;
    private static String           str_tab_text_count;

    
    //Drop down menus
    private static ArrayList<String>        al_market;
    private static String[]                 arr_str_market;
    private static String                   str_market_old;
    private static Boolean                  bol_init_market = true;
    private static ArrayList<String>        al_family_awc;
    private static String[]                 arr_str_family_awc;
    private static ArrayList<String>        al_family_iwc;
    private static String[]                 arr_str_family_iwc;
    private static ArrayList<String>        al_family_rwc;
    private static String[]                 arr_str_family_rwc;
    private static ArrayList<String>        al_family_all;
    private static String[]                 arr_str_family_all;
    private static ArrayList<String>        al_family_nothing_chosen;
    private static String                   str_family_nothing_chosen;
    private static Boolean                  bol_init_family = true;
    private static String[]                 arr_str_current_crosssection;
    private static ArrayList<String>        al_current_crosssection;
    private static String[]                 arr_str_current_crosssection_value;
    private static ArrayList<String>        al_current_crosssection_value;
    private static String[]                 arr_str_current_crosssection_value_a;
    private static ArrayList<String>        al_current_crosssection_value_a;
    private static String[]                 arr_str_current_crosssection_value_mm;
    private static ArrayList<String>        al_current_crosssection_value_mm;
    private static String[]                 arr_str_current_crosssection_value_awg;
    private static ArrayList<String>        al_current_crosssection_value_awg;
    private static Boolean                  bol_init_current_crosssection = true;
    private static String[]                 arr_str_sc_mc;
    private static ArrayList<String>        al_sc_mc;
    private static ArrayList<String>        al_sc_mc_nothing_chosen;
    private static String                   str_sc_mc_nothing_chosen;
    private static Boolean                  bol_init_sc_mc = true;
    private static String[]                 arr_str_mc_core_number;
    private static ArrayList<String>        al_mc_core_number;
    private static Boolean                  bol_init_mc_core_number = true;
    private static String[]                 arr_str_frequency;
    private static ArrayList<String>        al_frequency;
    private static Boolean                  bol_init_frequency = true;
    private static String[]                 arr_str_conductor_temperature;
    private static ArrayList<String>        al_conductor_temperature;
    private static Boolean                  bol_init_conductor_temperature = true;
    private static Boolean                  bol_safe_conductor_temperature = true;
    private static String[]                 arr_str_ambient_temperature;
    private static ArrayList<String>        al_ambient_temperature;
    private static Boolean                  bol_init_ambient_temperature = true;
    private static String[]                 arr_str_ambient_installation_mode;
    private static ArrayList<String>        al_installation_mode;
    private static Boolean                  bol_init_installation_mode = true;
    private static String[]                 arr_str_count;
    private static ArrayList<String>        al_count;
    private static Boolean                  bol_init_count = true;
    private static String                   str_result;
    private static String                   str_textfield_1;
    private static String[]                 str_arr_nominal_currents;
    private static String PRE_HTML_black = "<html><font color=\"black\"><p style=\"text-align: left; width: 125px\">"; //tappedpane text alignment  <font color="red">This is some text!</font> 
    private static String POST_HTML_black = "</p></font></html>"; //tappedpane text alignment
    private static String PRE_HTML_grey = "<html><p style=\"text-align: left; width: 125px\"><font color=\"gray\">"; //tappedpane text alignment  <font color="red">This is some text!</font> 
    private static String POST_HTML_grey = "</font></p></html>"; //tappedpane text alignment
    private static String str_tab;
    private static String str_tab_textfield;
    
    //Textfields
    private static JTextField   tf_input_text;
    private static JTextField   tf_result;
    
    //general variables
    private static int i;
    private static int j = 0;
    private static int tab;
    private static String str_step_old;
    private static Boolean bol_init = false;
    private static Boolean bol_init_cable_data;
    private static Boolean bol_run;

    
    //calculation variables
    private static double d_current;
    private static double d_current_operating;
    private static double d_current_delta;
    private static double d_current_nominal;
    private static double d_factor_core_number;
    private static double d_factor_ambient_temperature;
    private static double d_factor_conductor_temperature;
    private static double d_factor_frequency;
    private static double d_factor_installation_mode;
    private static double d_result;
    
    //new cable/core
    private static Cable wire_cable;    //Buffermemory for chosen cable data's from the gui
    
    //Database for Cables
    private static AWC_Cables awc_families;
    private static IWC_Cables iwc_families;
    private static RWC_Cables rwc_families;
    
    public CurrentCalc(){
        add(init_GUI());
        init_family_data();         //load all cablefamilies into the dropdown list
        wire_cable = new Cable();   //init of buffermemory for chosen cable data's from the gui
        bol_init = true;
        bol_init_cable_data = true; //blocks actionlistener during cable data loading
    }
    
    public static void main(String[] args){
        new CurrentCalc();
    }
    
    public JTabbedPane init_GUI() {
         //Load Design
         ////UIManager.put("nimbusBase", new Color(121,198,255));
         //UIManager.put("nimbusBlueGrey", new Color(121,198,255));
         ////UIManager.put("control", new Color(121,198,255));
         try {
             for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                 if ("Nimbus".equals(info.getName())) {
                     UIManager.setLookAndFeel(info.getClassName());
                     break;
                 }
             }
         } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
         }
         
         tp_main = new JTabbedPane(JTabbedPane.LEFT,JTabbedPane.SCROLL_TAB_LAYOUT);
         tp_main.addChangeListener(new ChangeListener() {
             public void stateChanged(ChangeEvent e) {
                 tab_action();
                }
         });

         //market tab //jpr_step_market.setBorder(new LineBorder(Color.BLACK));
         jpr_step_market = new JPanel_Right("Step 1","Choose your market.", this,this,this);
         tp_main.addTab(set_tab_text("1. &nbsp  Market","-","black"), jpr_step_market);
         jpr_step_market.set_combobox_1_visibility(true);
         jpr_step_market.set_combobox_2_visibility(false);
         jpr_step_market.set_combobox_3_visibility(false);
         jpr_step_market.set_textfield_1_visibility(false);
         jpr_step_market.set_installation_mode_pics_visible(false);
         //family tab
         jpr_step_family = new JPanel_Right("Step 2","Choose your cable family.", this,this,this);
         tp_main.addTab(set_tab_text("2. &nbsp  Family","-","black"), jpr_step_family);
         jpr_step_family.set_combobox_1_visibility(true);
         jpr_step_family.set_combobox_2_visibility(false);
         jpr_step_family.set_combobox_3_visibility(false);
         jpr_step_family.set_textfield_1_visibility(false);
         jpr_step_family.set_installation_mode_pics_visible(false);
         ///singlecore chooses
         jpr_step_sc_mc = new JPanel_Right("Step 3","Do you have a multicore or singlecore cable?", this,this,this);
         tp_main.addTab(set_tab_text("3. &nbsp  Singlecore or Multicore","-","black"), jpr_step_sc_mc);
         jpr_step_sc_mc.set_combobox_1_visibility(true);
         jpr_step_sc_mc.set_combobox_2_visibility(false);
         jpr_step_sc_mc.set_combobox_3_visibility(false);
         jpr_step_sc_mc.set_textfield_1_visibility(false);
         jpr_step_sc_mc.set_installation_mode_pics_visible(false);
         //amount of cores in multicore cable choose
         jpr_step_mc_core_number = new JPanel_Right("Step 4","How many cores does your cable have?", this,this,this);
         tp_main.addTab(set_tab_text("4. &nbsp Multicore Corenumber","-","gray"), jpr_step_mc_core_number);
         tp_main.setEnabledAt(3, false);
         jpr_step_mc_core_number.set_combobox_1_visibility(true);
         jpr_step_mc_core_number.set_combobox_2_visibility(false);
         jpr_step_mc_core_number.set_combobox_3_visibility(false);
         jpr_step_mc_core_number.set_textfield_1_visibility(false);
         jpr_step_mc_core_number.set_installation_mode_pics_visible(false);
         //calculation mode tab
         jpr_step_current_crosssection = new JPanel_Right("Step 5","With what do you want to calculate with?", this,this,this);
         tp_main.addTab(set_tab_text("5. &nbsp Crosssection/Current","-","gray"), jpr_step_current_crosssection);
         tp_main.setEnabledAt(4, false);
         jpr_step_current_crosssection.set_combobox_1_visibility(true);
         jpr_step_current_crosssection.set_combobox_2_visibility(true);
         jpr_step_current_crosssection.set_combobox_3_visibility(true);
         jpr_step_current_crosssection.set_textfield_1_visibility(false);
         jpr_step_current_crosssection.set_installation_mode_pics_visible(false);
         //frequency tab
         jpr_step_frequency = new JPanel_Right("Step 6","What is the frequency of the current? [Hz]", this,this,this);
         tp_main.addTab(set_tab_text("6. &nbsp Current Frequency","-","gray"), jpr_step_frequency);
         tp_main.setEnabledAt(5, false);
         jpr_step_frequency.set_combobox_1_visibility(true);
         jpr_step_frequency.set_combobox_2_visibility(false);
         jpr_step_frequency.set_combobox_3_visibility(false);
         jpr_step_frequency.set_textfield_1_visibility(false);
         jpr_step_frequency.set_installation_mode_pics_visible(false);
         //conductor temperature tab
         jpr_step_conductor_temperature = new JPanel_Right("Step 7","What is the conductor temperature? [�C]", this,this,this);
         tp_main.addTab(set_tab_text("7. &nbsp Conductor Temperature","-","gray"), jpr_step_conductor_temperature);
         tp_main.setEnabledAt(6, false);
         jpr_step_conductor_temperature.set_combobox_1_visibility(true);
         jpr_step_conductor_temperature.set_combobox_2_visibility(false);
         jpr_step_conductor_temperature.set_combobox_3_visibility(false);
         jpr_step_conductor_temperature.set_textfield_1_visibility(false);
         jpr_step_conductor_temperature.set_installation_mode_pics_visible(false);
         //ambient temperature tab
         jpr_step_ambient_temperature = new JPanel_Right("Step 8","What is the ambient temperature? [�C]", this,this,this);
         tp_main.addTab(set_tab_text("8. &nbsp Ambient Temperature","-","gray"), jpr_step_ambient_temperature);
         tp_main.setEnabledAt(7, false);
         jpr_step_ambient_temperature.set_combobox_1_visibility(true);
         jpr_step_ambient_temperature.set_combobox_2_visibility(false);
         jpr_step_ambient_temperature.set_combobox_3_visibility(false);
         jpr_step_ambient_temperature.set_textfield_1_visibility(false);
         jpr_step_ambient_temperature.set_installation_mode_pics_visible(false);
         //installation mode tab
         jpr_step_installation_mode = new JPanel_Right("Step 9","Choose your installation mode.", this,this,this);
         tp_main.addTab(set_tab_text("9. &nbsp Installation Mode","-","gray"), jpr_step_installation_mode);
         tp_main.setEnabledAt(8, false);
         jpr_step_installation_mode.set_combobox_1_visibility(false);
         jpr_step_installation_mode.set_combobox_2_visibility(false);
         jpr_step_installation_mode.set_combobox_3_visibility(false);
         jpr_step_installation_mode.set_textfield_1_visibility(false);
         jpr_step_installation_mode.set_installation_mode_pics_visible(true);
         //amount of next lying cables tab
         jpr_step_count = new JPanel_Right("Step 10","How many cables are next togheter?", this,this,this);
         tp_main.addTab(set_tab_text("10. Number of Cables","-","gray"), jpr_step_count);
         tp_main.setEnabledAt(9, false);
         jpr_step_count.set_combobox_1_visibility(true);
         jpr_step_count.set_combobox_2_visibility(false);
         jpr_step_count.set_combobox_3_visibility(false);
         jpr_step_count.set_textfield_1_visibility(false);
         jpr_step_count.set_installation_mode_pics_visible(false);
         
         tp_main.setVisible(true);  
         return tp_main;
    }// end init        

    public void step_market(){  
    }
    
    public void step_family(){
        //init data into cb_dropdown_1 list
        if(bol_init_family){
            switch(wire_cable.get_market()){
                case "Automotive":      jpr_step_family.load_combobox_1(al_family_awc);
                                        System.out.println("load awc list: "+wire_cable.get_market());
                break;
                case "Industrial":      jpr_step_family.load_combobox_1(al_family_iwc);
                                        System.out.println("load iwc list: "+wire_cable.get_market());
                break;
                case "Railway":         jpr_step_family.load_combobox_1(al_family_rwc);
                                        System.out.println("load rwc list: "+wire_cable.get_market());
                break;
                case "All":             jpr_step_family.load_combobox_1(al_family_all);
                                        System.out.println("load all list: "+wire_cable.get_market());
                break;
                default:                jpr_step_family.load_combobox_1(al_family_nothing_chosen);
                                        System.out.println("load not list: "+wire_cable.get_market());
            }
            bol_init_family = false;
        }
    }
    
    public void step_sc_mc(){
        if(bol_init_sc_mc){
            if(wire_cable.get_market() != "choose" && wire_cable.get_family() != "choose" && wire_cable.get_family() != "no market has been chosen"){
                al_sc_mc = load_cable_family_sc_mc();
                jpr_step_sc_mc.load_combobox_1(al_sc_mc);
            }else{
                jpr_step_sc_mc.load_combobox_1(al_sc_mc_nothing_chosen);
            }
            bol_init_sc_mc = false;
        }
    }
    
    public void step_mc_core_number(){
    }
    
    public void step_current_crosssection(String str_current_crosssection){
        //cb_dropdown_2 + cb_dropdown_3 list
        switch(str_current_crosssection){
            case "Current [A]":                             jpr_step_current_crosssection.set_combobox_2_visibility(false);
                                                            jpr_step_current_crosssection.set_combobox_3_visibility(false);
                                                            jpr_step_current_crosssection.set_textfield_1_visibility(true);
                                                            System.out.println("show current");
            break;
            case "Cross Section [mm2]":                     jpr_step_current_crosssection.set_combobox_2_visibility(true);
                                                            jpr_step_current_crosssection.set_combobox_3_visibility(false);
                                                            jpr_step_current_crosssection.set_textfield_1_visibility(false);
                                                            System.out.println("show cross section");
            break;
            case "Cross Section [AWG]":                     jpr_step_current_crosssection.set_combobox_2_visibility(false);
                                                            jpr_step_current_crosssection.set_combobox_3_visibility(true);
                                                            jpr_step_current_crosssection.set_textfield_1_visibility(false);
            break;
            default:                                        jpr_step_current_crosssection.set_combobox_2_visibility(false);
                                                            jpr_step_current_crosssection.set_combobox_3_visibility(true);
                                                            jpr_step_current_crosssection.set_textfield_1_visibility(false);
        }
    }
    
    public void step_frequency(){
    }
    
    public void step_conductor_temperature(){
    }
    
    public void step_ambient_temperature(){
    }
    
    public void step_installation_mode(){
    }
    
    public void step_count(){
    }
    
    /**
     * this method initializes all cable families and loads them into the cb_dropdown_1 list
     */
    public void init_family_data(){
        awc_families = new AWC_Cables();
        iwc_families = new IWC_Cables();
        rwc_families = new RWC_Cables();
        
        //market data dropdown content
        arr_str_market = new String[]{"choose","Industrial","Railway","Automotive"};
        al_market = new ArrayList<String>();
        Collections.addAll(al_market, arr_str_market);
        jpr_step_market.load_combobox_1(al_market);     //load into dropdownlist

        
        //load awc families into a string array for the dropdownlist
        arr_str_family_awc = awc_families.load_awc();
        al_family_awc = new ArrayList<String>();
        Collections.addAll(al_family_awc, arr_str_family_awc); 
        
        //load iwc families into a string array for the dropdownlist
        arr_str_family_iwc = iwc_families.load_iwc();
        al_family_iwc = new ArrayList<String>();
        Collections.addAll(al_family_iwc, arr_str_family_iwc); 
        
        //load rwc families into a string array for the dropdownlist
        arr_str_family_rwc = rwc_families.load_rwc();
        al_family_rwc = new ArrayList<String>();
        Collections.addAll(al_family_rwc, arr_str_family_rwc); 
        
        //load all families into a string array
        arr_str_family_all = new String[arr_str_family_awc.length+arr_str_family_iwc.length+arr_str_family_rwc.length];
        System.arraycopy(arr_str_family_awc, 0, arr_str_family_all, 0, arr_str_family_awc.length);
        System.arraycopy(arr_str_family_iwc, 0, arr_str_family_all, arr_str_family_awc.length, arr_str_family_iwc.length);
        System.arraycopy(arr_str_family_rwc, 0, arr_str_family_all, arr_str_family_awc.length+arr_str_family_iwc.length, arr_str_family_rwc.length);
        al_family_all = new ArrayList<String>();
        Collections.addAll(al_family_all, arr_str_family_all);
        al_family_all.remove("choose");
        al_family_all.remove("choose");
        al_family_all.remove("choose");
        al_family_all.add(0, "choose");
        
        //no market segment has been chosen
        str_family_nothing_chosen = "no market has been chosen";
        al_family_nothing_chosen = new ArrayList<String>();
        Collections.addAll(al_family_nothing_chosen, str_family_nothing_chosen); 
        
        //no family has been chosen
        str_sc_mc_nothing_chosen = "no family has been chosen";
        al_sc_mc_nothing_chosen = new ArrayList<String>();
        Collections.addAll(al_sc_mc_nothing_chosen, str_sc_mc_nothing_chosen); 
    }
    
    /**
     * this method checks, if chosen family contents also multicore cables
     */
    public ArrayList load_cable_family_sc_mc() {
        if(wire_cable.get_market() != "choose" && wire_cable.get_family() != "choose"){
            //load family into dropdowns "Industrial","Railway","Automotive","All"
            switch(wire_cable.get_market()){
                case "Automotive":      arr_str_sc_mc = awc_families.load_sc_mc(wire_cable.get_family());
                break;
                case "Industrial":      arr_str_sc_mc = iwc_families.load_sc_mc(wire_cable.get_family());
                break;
                case "Railway":         arr_str_sc_mc = rwc_families.load_sc_mc(wire_cable.get_family());
                break;
                default:;
            }
        }else{
            //do nothing?
        }
        al_sc_mc = new ArrayList<String>();
        Collections.addAll(al_sc_mc, arr_str_sc_mc); 
        return al_sc_mc;
    }
    
    /**
     * this method initializes all data for the tables and cb_dropdown_1 lists
     */
    public void init_data(String str_chosen_cable_market, String str_chosen_cable_family, String str_chosen_sc_mc){
        //load data and setup default values
        switch(str_chosen_cable_market){
            case "Automotive":      //init the cable!
                                    awc_families.init_cable_family(str_chosen_cable_family, str_chosen_sc_mc);
            
                                    //init all data from the chosen cable family
                                    arr_str_current_crosssection = awc_families.load_current_crosssection();
                                    wire_cable.set_current_crosssection(awc_families.get_default_current_crosssection(),awc_families.get_default_crosssection());
                                    
                                    //arr_str_current_crosssection_value_a = awc_families.load_current();
                                    
                                    arr_str_current_crosssection_value_mm = awc_families.load_crosssections_mm();
                                    
                                    arr_str_current_crosssection_value_awg = awc_families.load_crosssections_awg();
                                    
                                    arr_str_mc_core_number = awc_families.load_mc_core_number();
                                    wire_cable.set_mc_core_number(awc_families.get_default_mc_core_number());
                                    
                                    arr_str_ambient_temperature = awc_families.load_ambient_temp();
                                    wire_cable.set_ambient_temperature(awc_families.get_default_ambient_temperature());

                                    arr_str_conductor_temperature = awc_families.load_conductor_temp();
                                    wire_cable.set_conductor_temperature(awc_families.get_default_conductor_temperature());

                                    arr_str_frequency = awc_families.load_frequency();
                                    wire_cable.set_frequency(awc_families.get_default_frequency());

                                    arr_str_ambient_installation_mode = awc_families.load_installation_mode();
                                    wire_cable.set_installation_mode(awc_families.get_default_inst_mode());

                                    arr_str_count = awc_families.load_nr_of_other_cables();   
                                    wire_cable.set_count(awc_families.get_default_number_of_other_cables());
            break;
            
            case "Industrial":      //init the cable!
                                    iwc_families.init_cable_family(str_chosen_cable_family, str_chosen_sc_mc);
            
                                    //init all data from the chosen cable family
                                    arr_str_current_crosssection = iwc_families.load_current_crosssection();
                                    wire_cable.set_current_crosssection(iwc_families.get_default_current_crosssection(),iwc_families.get_default_crosssection());
                                    
                                    //arr_str_current_crosssection_value_a = iwc_families.load_current();
                                    
                                    arr_str_current_crosssection_value_mm = iwc_families.load_crosssections_mm();
                                    
                                    arr_str_current_crosssection_value_awg = iwc_families.load_crosssections_awg();
                                    
                                    arr_str_mc_core_number = iwc_families.load_mc_core_number();
                                    wire_cable.set_mc_core_number(iwc_families.get_default_mc_core_number());
                                    
                                    arr_str_ambient_temperature = iwc_families.load_ambient_temp();
                                    wire_cable.set_ambient_temperature(iwc_families.get_default_ambient_temperature());

                                    arr_str_conductor_temperature = iwc_families.load_conductor_temp();
                                    wire_cable.set_conductor_temperature(iwc_families.get_default_conductor_temperature());

                                    arr_str_frequency = iwc_families.load_frequency();
                                    wire_cable.set_frequency(iwc_families.get_default_frequency());

                                    arr_str_ambient_installation_mode = iwc_families.load_installation_mode();
                                    wire_cable.set_installation_mode(iwc_families.get_default_inst_mode());

                                    arr_str_count = iwc_families.load_nr_of_other_cables();   
                                    wire_cable.set_count(iwc_families.get_default_number_of_other_cables());
            break;
            
            case "Railway":         //init the cable!
                                    rwc_families.init_cable_family(str_chosen_cable_family, str_chosen_sc_mc);
            
                                    //init all data from the chosen cable family
                                    arr_str_current_crosssection = rwc_families.load_current_crosssection();
                                    wire_cable.set_current_crosssection(rwc_families.get_default_current_crosssection(),rwc_families.get_default_crosssection());
                                    
                                    //arr_str_current_crosssection_value_a = rwc_families.load_current();
                                    
                                    arr_str_current_crosssection_value_mm = rwc_families.load_crosssections_mm();
                                    
                                    arr_str_current_crosssection_value_awg = rwc_families.load_crosssections_awg();
                                    
                                    arr_str_mc_core_number = rwc_families.load_mc_core_number();
                                    wire_cable.set_mc_core_number(rwc_families.get_default_mc_core_number());
                                    
                                    arr_str_ambient_temperature = rwc_families.load_ambient_temp();
                                    wire_cable.set_ambient_temperature(rwc_families.get_default_ambient_temperature());

                                    arr_str_conductor_temperature = rwc_families.load_conductor_temp();
                                    wire_cable.set_conductor_temperature(rwc_families.get_default_conductor_temperature());

                                    arr_str_frequency = rwc_families.load_frequency();
                                    wire_cable.set_frequency(rwc_families.get_default_frequency());

                                    arr_str_ambient_installation_mode = rwc_families.load_installation_mode();
                                    wire_cable.set_installation_mode(rwc_families.get_default_inst_mode());

                                    arr_str_count = rwc_families.load_nr_of_other_cables();   
                                    wire_cable.set_count(rwc_families.get_default_number_of_other_cables());
            break;
            default:;
        }
        bol_init_cable_data = false;    //disable actions in actionlistener until finished loading dropdowns
        //cable core count dropdown content
        al_mc_core_number = new ArrayList<String>();
        Collections.addAll(al_mc_core_number, arr_str_mc_core_number); 
        jpr_step_mc_core_number.load_combobox_1(al_mc_core_number);     //load core numbers into dropdown
        jpr_step_mc_core_number.set_combobox_1(wire_cable.get_mc_core_number()); //set default value after init
        //crossection current dropdown content
        al_current_crosssection = new ArrayList<String>();
        Collections.addAll(al_current_crosssection, arr_str_current_crosssection); 
        jpr_step_current_crosssection.load_combobox_1(al_current_crosssection);     //load calculation modes into dropdown
        jpr_step_current_crosssection.set_combobox_1(wire_cable.get_current_crosssection()); //set default value after init
        //cross current according to table into dropdown content
        //al_current_crosssection_value_a = new ArrayList<String>();
        //Collections.addAll(al_current_crosssection_value_a, arr_str_current_crosssection_value_a); 
        //loading cross section in mm into dropdown content
        al_current_crosssection_value_mm = new ArrayList<String>();
        Collections.addAll(al_current_crosssection_value_mm, arr_str_current_crosssection_value_mm); 
        jpr_step_current_crosssection.load_combobox_2(al_current_crosssection_value_mm);     //load cross section into dropdown
        jpr_step_current_crosssection.set_combobox_2(wire_cable.get_current_crosssection_value()); //set default value after init
        //loading cross section in awg into dropdown content
        al_current_crosssection_value_awg = new ArrayList<String>();
        Collections.addAll(al_current_crosssection_value_awg, arr_str_current_crosssection_value_awg); 
        jpr_step_current_crosssection.load_combobox_3(al_current_crosssection_value_awg);     //load cross section into dropdown
        jpr_step_current_crosssection.set_combobox_3(wire_cable.get_current_crosssection_value()); //set default value after init
        //frequency dropdown content
        al_frequency = new ArrayList<String>();
        Collections.addAll(al_frequency, arr_str_frequency); 
        jpr_step_frequency.load_combobox_1(al_frequency);     //load frequency into dropdown
        jpr_step_frequency.set_combobox_1(wire_cable.get_frequency()); //set default value after init
        //conductor temperature dropdown content
        al_conductor_temperature = new ArrayList<String>();
        Collections.addAll(al_conductor_temperature, arr_str_conductor_temperature); 
        jpr_step_conductor_temperature.load_combobox_1(al_conductor_temperature);     //load conductor temperature into dropdown
        jpr_step_conductor_temperature.set_combobox_1(wire_cable.get_conductor_temperature()); //set default value after init
        //ambient temperature dropdown content
        al_ambient_temperature = new ArrayList<String>();
        Collections.addAll(al_ambient_temperature, arr_str_ambient_temperature); 
        jpr_step_ambient_temperature.load_combobox_1(al_ambient_temperature);     //load ambient temperature into dropdown
        jpr_step_ambient_temperature.set_combobox_1(wire_cable.get_ambient_temperature()); //set default value after init
        //installation mode dropdown content
        al_installation_mode = new ArrayList<String>();
        Collections.addAll(al_installation_mode, arr_str_ambient_installation_mode); 
        jpr_step_installation_mode.load_combobox_1(al_installation_mode);     //load ambient temperature into dropdown
        jpr_step_installation_mode.set_combobox_1(wire_cable.get_installation_mode()); //set default value after init
        jpr_step_installation_mode.set_inst_icon(wire_cable.get_installation_mode());
        //cable core count of cables next to the focused cable dropdown content
        al_count = new ArrayList<String>();
        Collections.addAll(al_count, arr_str_count); 
        jpr_step_count.load_combobox_1(al_count);     //load ambient temperature into dropdown
        jpr_step_count.set_combobox_1(wire_cable.get_count()); //set default value after init
        //Result
        str_result = "not yet calculated";
        bol_init_cable_data = true;
    }
    
    /**
     * changelistener for the tabs
     * 0 = market
     * 1 = family
     * 2 = multicore/singlecore choosing
     * 3 = calculation mode: input with cross section or current
     * 4 = amount of cores for multicore cables
     * 5 = frequency
     * 6 = conductor temperature
     * 7 = ambient temperature
     * 8 = installation mode
     * 9 = amount of cables lying arround
     */
    private void tab_action(){
        tab = tp_main.getSelectedIndex();
        //add something necessary
        if(bol_init){
            switch(tab){
                case 0:     step_market();
                            jpr_step_market.set_textfield_result("Result"); //set result textfield to "result"
                break;
                case 1:     step_family();
                            jpr_step_family.set_textfield_result("Result"); //set result textfield to "result"
                break;
                case 2:     step_sc_mc();
                            jpr_step_sc_mc.set_textfield_result("Result"); //set result textfield to "result"
                break;
                case 3:     step_mc_core_number();
                            jpr_step_mc_core_number.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 4:     step_current_crosssection(wire_cable.get_current_crosssection());
                            jpr_step_current_crosssection.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 5:     step_frequency();
                            jpr_step_frequency.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 6:     step_conductor_temperature();
                            jpr_step_conductor_temperature.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 7:     step_ambient_temperature();
                            jpr_step_ambient_temperature.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 8:     step_installation_mode();
                            jpr_step_installation_mode.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 9:     step_count();
                            jpr_step_count.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                default:;
            }
        }
    }
    
    /**
     * actionlistener for the dropdowns in the JPANEL_Right classes to save data
     * 0 = market
     * 1 = family
     * 2 = multicore/singlecore choosing
     * 3 = amount of cores for multicore cables
     * 4 = calculation mode: input with cross section or current
     * 5 = frequency
     * 6 = conductor temperature
     * 7 = ambient temperature
     * 8 = installation mode
     * 9 = amount of cables lying arround
     */
    public void actionPerformed(ActionEvent e) {
        tab = tp_main.getSelectedIndex();
            if(bol_init){
            switch(tab){
                case 0:     if(wire_cable.get_market() != jpr_step_market.get_dropdown_1()){    //reset families list, if a new market or singlecore multicore has been selected
                                bol_init_family = true;
                                bol_init_sc_mc = true;
                                tp_main.setTitleAt(1,PRE_HTML_black+"<html><body>2. &nbsp  Family:<br>&nbsp &nbsp &nbsp -</body></html>"+PRE_HTML_black);
                                tp_main.setTitleAt(2,PRE_HTML_black+"<html><body>3. &nbsp Singlecore or Multicore:<br>&nbsp &nbsp &nbsp -</body></html>"+PRE_HTML_black);
                                set_cable_tabs_3_9(false);
                                wire_cable.reset_data();    //empty cable data buffer
                            }
                            wire_cable.set_market(jpr_step_market.get_dropdown_1());    //save selected item from dropdown list
                            if(jpr_step_market.get_dropdown_1() != "choose"){
                                jpr_step_market.remove_combobox_1_item("choose");       //remove "choose" item, if something else has been selected
                                set_tab_text_at(0, jpr_step_market.get_dropdown_1());   //refresh tab text
                                tp_main.setSelectedIndex(1);
                            }
                break;
                case 1:     if(wire_cable.get_family() != jpr_step_family.get_dropdown_1()){    //reset singlecore multicore list, if a new market or family has been selected
                                bol_init_sc_mc = true;
                                set_cable_tabs_3_9(false);
                                tp_main.setTitleAt(2,PRE_HTML_black+"<html><body>3. &nbsp Singlecore or Multicore:<br>&nbsp &nbsp &nbsp -</body></html>"+PRE_HTML_black);
                                //wire_cable.reset_data();    //empty cable data buffer
                            }
                            wire_cable.set_family(jpr_step_family.get_dropdown_1());    //save selected item from dropdown list
                            if(jpr_step_family.get_dropdown_1() != "choose" && jpr_step_family.get_dropdown_1() != "no market has been chosen"){
                                jpr_step_family.remove_combobox_1_item("choose");       //remove "choose" item, if something else has been selected
                                set_tab_text_at(1, jpr_step_family.get_dropdown_1());   //refresh tab text
                                tp_main.setSelectedIndex(2);
                            }
                break;
                case 2:     if(bol_init_cable_data){    //action needs to be blocked during cable data loading!
                                wire_cable.set_sc_mc(jpr_step_sc_mc.get_dropdown_1());
                                if(jpr_step_sc_mc.get_dropdown_1() != "choose" && jpr_step_sc_mc.get_dropdown_1() != "no family has been chosen"){
                                    jpr_step_sc_mc.remove_combobox_1_item("choose");       //remove "choose" item, if something else has been selected
                                    set_tab_text_at(2, jpr_step_sc_mc.get_dropdown_1());   //refresh tab text
                                    init_data(wire_cable.get_market(),wire_cable.get_family(),wire_cable.get_sc_mc()); //load all data from chosen cablefamily into dropdown lists
                                    set_cable_tabs_3_9(true);
                                    jpr_step_sc_mc.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                                    jpr_step_mc_core_number.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result before jump into mc core number tab
                                }else{
                                    //jpr_step_sc_mc.set_textfield_result("Result");
                                }
                            }
                break;
                case 3:     wire_cable.set_mc_core_number(jpr_step_mc_core_number.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(3, jpr_step_mc_core_number.get_dropdown_1());   //refresh tab text
                            jpr_step_mc_core_number.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 4:     wire_cable.set_current_crosssection(jpr_step_current_crosssection.get_dropdown_1(),jpr_step_current_crosssection.get_dropdown_2()); //save selected item from dropdown into cable object
                            step_current_crosssection(wire_cable.get_current_crosssection());   
                            if(wire_cable.get_current_crosssection() == "Cross Section [mm2]"){
                                set_tab_text_at(4, jpr_step_current_crosssection.get_dropdown_2()+" mm2");   //refresh tab text
                            }else{
                                if(jpr_step_current_crosssection.get_textfield_1() == null){
                                    set_tab_text_at(4, " A");   //refresh tab text
                                }else{
                                    set_tab_text_at(4, jpr_step_current_crosssection.get_textfield_1()+" A");   //refresh tab text
                                }
                            }
                            jpr_step_current_crosssection.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 5:     wire_cable.set_frequency(jpr_step_frequency.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(5, jpr_step_frequency.get_dropdown_1()+" Hz");   //refresh tab text
                            jpr_step_frequency.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 6:     wire_cable.set_conductor_temperature(jpr_step_conductor_temperature.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(6, jpr_step_conductor_temperature.get_dropdown_1()+" �C");   //refresh tab text
                            jpr_step_conductor_temperature.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 7:     wire_cable.set_ambient_temperature(jpr_step_ambient_temperature.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(7, jpr_step_ambient_temperature.get_dropdown_1()+" �C");   //refresh tab text
                            jpr_step_ambient_temperature.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 8:     wire_cable.set_installation_mode(jpr_step_installation_mode.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(8, jpr_step_installation_mode.get_dropdown_1());   //refresh tab text
                            jpr_step_installation_mode.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                case 9:     wire_cable.set_count(jpr_step_count.get_dropdown_1()); //save selected item from dropdown into cable object
                            set_tab_text_at(9, jpr_step_count.get_dropdown_1());   //refresh tab text
                            jpr_step_count.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(), str_textfield_1)); //refresh result
                break;
                default:;
            }
        }
        //String calculate_current()
    } // end actionPerformed()
    
    public void itemStateChanged(ItemEvent e){
        if(e.getStateChange() == ItemEvent.DESELECTED){
        }
    }

    public void changedUpdate(DocumentEvent f) {
        //The textfieldcalue has to be read delayed from the tab panel!
        new java.util.Timer().schedule( 
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        System.out.println("ich rechne mit: "+jpr_step_current_crosssection.get_textfield_1());
                        str_textfield_1 = jpr_step_current_crosssection.get_textfield_1();
                        jpr_step_current_crosssection.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(),str_textfield_1));
                        try{
                            set_tab_text_at(4, jpr_step_current_crosssection.get_textfield_1()+" A");   //refresh tab text
                        }catch(Exception r){
                        }
                    }
                }, 
                10 
        );
    }

    public void insertUpdate(DocumentEvent f) {
        //The textfieldcalue has to be read delayed from the tab panel!
        new java.util.Timer().schedule( 
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        System.out.println("ich rechne mit: "+jpr_step_current_crosssection.get_textfield_1());
                        str_textfield_1 = jpr_step_current_crosssection.get_textfield_1();
                        jpr_step_current_crosssection.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(),str_textfield_1));
                        try{
                            set_tab_text_at(4, jpr_step_current_crosssection.get_textfield_1()+" A");   //refresh tab text
                        }catch(Exception r){
                        }
                    }
                }, 
                10 
        );
    }
    
    public void removeUpdate(DocumentEvent f) {
        //The textfieldcalue has to be read delayed from the tab panel!
        new java.util.Timer().schedule( 
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        System.out.println("ich rechne mit: "+jpr_step_current_crosssection.get_textfield_1());
                        str_textfield_1 = jpr_step_current_crosssection.get_textfield_1();
                        jpr_step_current_crosssection.set_textfield_result(calculate(wire_cable.get_current_crosssection(), wire_cable.get_market(),str_textfield_1));
                        try{
                            set_tab_text_at(4, jpr_step_current_crosssection.get_textfield_1()+" A");   //refresh tab text
                        }catch(Exception r){
                        }
                    }
                }, 
                10 
        );
    }
    
    private static String calculate(String str_current_crosssection, String str_market, String str_textfield_1){
        switch(str_current_crosssection){
            case "Current [A]":             str_result = calculate_crosssection(str_market, str_textfield_1);
            break;
            
            case "Cross Section [mm2]":     str_result = calculate_current(str_market);
            break;
            
            default:                        str_result = "Error";
        }
        return str_result;
    }
    
    private static String calculate_crosssection(String str_market, String str_textfield_1) {
        switch(str_market){
            case "Automotive":          
                                        try{
                                            d_current_operating = Double.parseDouble(str_textfield_1);
                                            d_factor_conductor_temperature = Double.parseDouble(awc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(awc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            //d_factor_frequency = Double.parseDouble(awc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value())); //fehlerhaft
                                            d_factor_installation_mode = Double.parseDouble(awc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_current = d_current_operating / (d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_installation_mode);
                                            System.out.println("calculatet current without reduction factors: "+d_current);
                                            
                                            //looking for next nominal current
                                            str_arr_nominal_currents = awc_families.load_nominal_currents();
                                            bol_run = true;
                                            for(i=0; i<str_arr_nominal_currents.length; i++){
                                                d_current_delta = d_current - Double.parseDouble(str_arr_nominal_currents[i]);
                                                if(d_current_delta <= 0 && bol_run){
                                                    d_current_nominal = Double.parseDouble(str_arr_nominal_currents[i]);
                                                    bol_run = false;
                                                }else{
                                                    //do nothing
                                                }
                                            }
                                            System.out.println("gefunden bei: "+i+", current nom: "+String.valueOf(d_current_nominal));
                                            //now looking with nominal current for the matching cross section
                                            str_result = awc_families.get_crosssection(String.valueOf(d_current_nominal));
                                            System.out.println("result: "+str_result);
                                            //prepare output
                                            if(bol_run){
                                                str_result = "available cross sections too low";
                                            }else{
                                                str_result = str_result + " mm2";
                                            }
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                            System.out.println("fail");
                                        }
            break;
            case "Industrial":          
                                        try{
                                            d_current_operating = Double.parseDouble(str_textfield_1);
                                            d_factor_conductor_temperature = Double.parseDouble(iwc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(iwc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            //d_factor_frequency = Double.parseDouble(rwc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value())); //fehlerhaft
                                            d_factor_installation_mode = Double.parseDouble(iwc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_current = d_current_operating / (d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_installation_mode);
                                            System.out.println("calculatet current without reduction factors: "+d_current);
                                            
                                            //looking for next nominal current
                                            str_arr_nominal_currents = iwc_families.load_nominal_currents();
                                            bol_run = true;
                                            for(i=0; i<str_arr_nominal_currents.length; i++){
                                                d_current_delta = d_current - Double.parseDouble(str_arr_nominal_currents[i]);
                                                if(d_current_delta <= 0 && bol_run){
                                                    d_current_nominal = Double.parseDouble(str_arr_nominal_currents[i]);
                                                    bol_run = false;
                                                }else{
                                                    //do nothing
                                                }
                                            }
                                            System.out.println("gefunden bei: "+i+", current nom: "+String.valueOf(d_current_nominal));
                                            //now looking with nominal current for the matching cross section
                                            str_result = iwc_families.get_crosssection(String.valueOf(d_current_nominal));
                                            System.out.println("result: "+str_result);
                                            //prepare output
                                            if(bol_run){
                                                str_result = "available cross sections too low";
                                            }else{
                                                str_result = str_result + " mm2";
                                            }
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                            System.out.println("fail");
                                        }
            break;
            case "Railway":          
                                        try{
                                            d_current_operating = Double.parseDouble(str_textfield_1);
                                            d_factor_conductor_temperature = Double.parseDouble(rwc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(rwc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            //d_factor_frequency = Double.parseDouble(rwc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value())); //fehlerhaft
                                            d_factor_installation_mode = Double.parseDouble(rwc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_current = d_current_operating / (d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_installation_mode);
                                            System.out.println("calculatet current without reduction factors: "+d_current);
                                            
                                            //looking for next nominal current
                                            str_arr_nominal_currents = rwc_families.load_nominal_currents();
                                            bol_run = true;
                                            for(i=0; i<str_arr_nominal_currents.length; i++){
                                                d_current_delta = d_current - Double.parseDouble(str_arr_nominal_currents[i]);
                                                if(d_current_delta <= 0 && bol_run){
                                                    d_current_nominal = Double.parseDouble(str_arr_nominal_currents[i]);
                                                    bol_run = false;
                                                }else{
                                                    //do nothing
                                                }
                                            }
                                            System.out.println("gefunden bei: "+i+", current nom: "+String.valueOf(d_current_nominal));
                                            //now looking with nominal current for the matching cross section
                                            str_result = rwc_families.get_crosssection(String.valueOf(d_current_nominal));
                                            System.out.println("result: "+str_result);
                                            //prepare output
                                            if(bol_run){
                                                str_result = "available cross sections too low";
                                            }else{
                                                str_result = str_result + " mm2";
                                            }
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                            System.out.println("fail");
                                        }
            break;
            default:                    str_result = "no data";
        }
        return str_result;
    }
    
    private static String calculate_current(String str_market) {
        switch(str_market){
            case "Automotive":          
                                        try{
                                            d_current_nominal = Double.parseDouble(awc_families.get_current_nominal(wire_cable.get_current_crosssection_value()));
                                            d_factor_conductor_temperature = Double.parseDouble(awc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(awc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            d_factor_frequency = Double.parseDouble(awc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value()));
                                            d_factor_installation_mode = Double.parseDouble(awc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_factor_core_number = Double.parseDouble(awc_families.get_factor_core_number(wire_cable.get_mc_core_number()));
                                            d_result = d_current_nominal*d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_frequency*d_factor_installation_mode*d_factor_core_number;
                                            str_result = Math.round(d_result * 100.0) / 100.0 + " A";
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                        }
            break;
            case "Industrial":          
                                        try{
                                            d_current_nominal = Double.parseDouble(iwc_families.get_current_nominal(wire_cable.get_current_crosssection_value()));
                                            d_factor_conductor_temperature = Double.parseDouble(iwc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(iwc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            d_factor_frequency = Double.parseDouble(iwc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value()));
                                            d_factor_installation_mode = Double.parseDouble(iwc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_factor_core_number = Double.parseDouble(iwc_families.get_factor_core_number(wire_cable.get_mc_core_number()));
                                            d_result = d_current_nominal*d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_frequency*d_factor_installation_mode*d_factor_core_number;
                                            str_result = Math.round(d_result * 100.0) / 100.0 + " A";
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                        }
            break;
            case "Railway":          
                                        try{
                                            d_current_nominal = Double.parseDouble(rwc_families.get_current_nominal(wire_cable.get_current_crosssection_value()));
                                            d_factor_conductor_temperature = Double.parseDouble(rwc_families.get_factor_conductor_temperature(wire_cable.get_conductor_temperature()));
                                            d_factor_ambient_temperature = Double.parseDouble(rwc_families.get_factor_ambient_temperature(wire_cable.get_ambient_temperature()));
                                            d_factor_frequency = Double.parseDouble(rwc_families.get_factor_frequency(wire_cable.get_frequency(),wire_cable.get_current_crosssection_value()));
                                            d_factor_installation_mode = Double.parseDouble(rwc_families.get_factor_installation_mode(wire_cable.get_installation_mode(),wire_cable.get_count()));
                                            d_factor_core_number = Double.parseDouble(rwc_families.get_factor_core_number(wire_cable.get_mc_core_number()));
                                            d_result = d_current_nominal*d_factor_conductor_temperature*d_factor_ambient_temperature*d_factor_frequency*d_factor_installation_mode*d_factor_core_number;
                                            str_result = Math.round(d_result * 100.0) / 100.0 + " A";
                                        }catch(Exception j){
                                            str_result = "incalculable";
                                        }
            break;
            default:                    str_result = "no data";
        }
        return str_result;
    }
    
    private static void waitProcessing(int iWait){
        try {
            Thread.sleep(iWait);
        }catch(InterruptedException ie) {
        }
    }
    
    private void set_cable_tabs_3_9(Boolean bol_set){
        if(bol_set){
            tp_main.setEnabledAt(3, true);
            tp_main.setTitleAt(3,PRE_HTML_black+"<html><body>4. &nbsp Multicore Corenumber:<br>&nbsp &nbsp &nbsp "+wire_cable.get_mc_core_number()+"</body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(4, true);
            tp_main.setTitleAt(4,PRE_HTML_black+"<html><body>5. &nbsp Crosssection/Current:<br>&nbsp &nbsp &nbsp "+wire_cable.get_current_crosssection_value()+" mm2 </body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(5, true);
            tp_main.setTitleAt(5,PRE_HTML_black+"<html><body>6. &nbsp Current Frequency:<br>&nbsp &nbsp &nbsp "+wire_cable.get_frequency()+" Hz</body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(6, true);
            tp_main.setTitleAt(6,PRE_HTML_black+"<html><body>7. &nbsp Conductor Temperature:<br>&nbsp &nbsp &nbsp "+wire_cable.get_conductor_temperature()+" �C</body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(7, true);
            tp_main.setTitleAt(7,PRE_HTML_black+"<html><body>8. &nbsp Ambient Temperature:<br>&nbsp &nbsp &nbsp "+wire_cable.get_ambient_temperature()+" �C</body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(8, true);
            tp_main.setTitleAt(8,PRE_HTML_black+"<html><body>9. &nbsp Installation Mode:<br>&nbsp &nbsp &nbsp "+wire_cable.get_installation_mode()+"</body></html>"+POST_HTML_black);
            tp_main.setEnabledAt(9, true);
            tp_main.setTitleAt(9,PRE_HTML_black+"<html><body>10. Number of Cables:<br>&nbsp &nbsp &nbsp "+wire_cable.get_count()+"</body></html>"+POST_HTML_black);
        }else{
            tp_main.setEnabledAt(3, false);
            tp_main.setTitleAt(3,PRE_HTML_grey+"<html><body>4. &nbsp Multicore Corenumber:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(4, false);
            tp_main.setTitleAt(4,PRE_HTML_grey+"<html><body>5. &nbsp Crosssection/Current:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(5, false);
            tp_main.setTitleAt(5,PRE_HTML_grey+"<html><body>6. &nbsp Current Frequency:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(6, false);
            tp_main.setTitleAt(6,PRE_HTML_grey+"<html><body>7. &nbsp Conductor Temperature:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(7, false);
            tp_main.setTitleAt(7,PRE_HTML_grey+"<html><body>8. &nbsp Ambient Temperature:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(8, false);
            tp_main.setTitleAt(8,PRE_HTML_grey+"<html><body>9. &nbsp Installation Mode:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
            tp_main.setEnabledAt(9, false);
            tp_main.setTitleAt(9,PRE_HTML_grey+"<html><body>10. Number of Cables:<br>&nbsp &nbsp &nbsp -</body></html>"+POST_HTML_grey);
        }
    }
    
    private void set_tab_text_at(int f, String str_text){
        switch(f){
                case 0:     tp_main.setTitleAt(0,PRE_HTML_black+"<html><body>1. &nbsp  Market:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+POST_HTML_black);
                break;
                case 1:     tp_main.setTitleAt(1,PRE_HTML_black+"<html><body>2. &nbsp  Family:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 2:     tp_main.setTitleAt(2,PRE_HTML_black+"<html><body>3. &nbsp Singlecore or Multicore:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 3:     tp_main.setTitleAt(3,PRE_HTML_black+"<html><body>4. &nbsp Multicore Corenumber:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 4:     tp_main.setTitleAt(4,PRE_HTML_black+"<html><body>5. &nbsp Crosssection / Current:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 5:     tp_main.setTitleAt(5,PRE_HTML_black+"<html><body>6. &nbsp Current Frequency:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 6:     tp_main.setTitleAt(6,PRE_HTML_black+"<html><body>7. &nbsp Conductor Temperature:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 7:     tp_main.setTitleAt(7,PRE_HTML_black+"<html><body>8. &nbsp Ambient Temperature:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 8:     tp_main.setTitleAt(8,PRE_HTML_black+"<html><body>9. &nbsp Installation Mode:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                case 9:     tp_main.setTitleAt(9,PRE_HTML_black+"<html><body>10. Number of Cables:<br>&nbsp &nbsp &nbsp "+str_text+"</body></html>"+PRE_HTML_black);
                break;
                default:;
        }
    }
    
    private String set_tab_text(String str_header, String str_content, String str_color){
        switch(str_color){
            case "gray":      str_tab = PRE_HTML_grey+str_header+":<br>&nbsp &nbsp &nbsp "+str_content+PRE_HTML_grey;
            break;
            
            case "black":     str_tab = PRE_HTML_black+str_header+":<br>&nbsp &nbsp &nbsp "+str_content+PRE_HTML_black;
            break;
        
            default:          str_tab = "error";
        }
        return str_tab;
    }

} // end class